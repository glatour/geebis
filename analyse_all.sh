#!/usr/bin/env bash

/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py complex fb15k237 pretrained noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py complex wn18rr pretrained noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py complex yago3-10 3997a9c047ee1ce2fa9ab991a3fb39934e3279696acd3447c98ecdfacae2e29a noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py complex fb15k237 pretrained default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py complex wn18rr pretrained default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py complex yago3-10 3997a9c047ee1ce2fa9ab991a3fb39934e3279696acd3447c98ecdfacae2e29a default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py transe fb15k237 pretrained noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py transe wn18rr pretrained noconst -vvv &
wait
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py transe yago3-10 1e3514dbdc7462f4a93e1d4748dd1129f080d3e153f84bcdc2cf0356cb8e8249 noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py transe fb15k237 pretrained default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py transe wn18rr pretrained default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py transe yago3-10 1e3514dbdc7462f4a93e1d4748dd1129f080d3e153f84bcdc2cf0356cb8e8249 default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py convkb fb15k237 0fbcade8e60673766a97b77fef5adf156947ddd26cbc53014cf1de395d1719fa noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py convkb wn18rr ba9b2bc5d71cf5aeae0e8348a211a3ade00ed62e4f2f6a873b364a19a5cb0a32 noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py convkb yago3-10 0d8f749aebdeecebf506652719cbedc3f074838886450a3008111c73f0cc779d noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py convkb fb15k237 0fbcade8e60673766a97b77fef5adf156947ddd26cbc53014cf1de395d1719fa default -vvv &
wait
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py convkb wn18rr ba9b2bc5d71cf5aeae0e8348a211a3ade00ed62e4f2f6a873b364a19a5cb0a32 default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py convkb yago3-10 0d8f749aebdeecebf506652719cbedc3f074838886450a3008111c73f0cc779d default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py hole fb15k237 8405323af2dd26e31babafc7adb3f8154020dfc9f794a767139b2d34b8c52684 noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py hole wn18rr c48074e2f8d19a454d89bdc28a6eea379365c1ee6b3cef5284a1aeea053bb750 noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py hole yago3-10 e96fe144bfb7ad5eb2dbc61a9644f9ea91e9b9c2db2beee32acdacb4297295ac noconst -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py hole fb15k237 8405323af2dd26e31babafc7adb3f8154020dfc9f794a767139b2d34b8c52684 default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py hole wn18rr c48074e2f8d19a454d89bdc28a6eea379365c1ee6b3cef5284a1aeea053bb750 default -vvv &
/home/lgalarra/Documents/git/geebis/venv/bin/python /home/lgalarra/Documents/git/geebis/src/analyse.py hole yago3-10 e96fe144bfb7ad5eb2dbc61a9644f9ea91e9b9c2db2beee32acdacb4297295ac default -vvv &
wait