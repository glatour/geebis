#!/usr/bin/python3

"""
Gee Rule Comparator

Usage:
    analyse <model1> <model2> <dataset> <hash1> <hash2> <profile> [options] [--verbose | -v | -vv | -vvv]
    analyse -h | --help

Arguments:
    <dataset>           Any of the directory names under resources/data.
                        The program automatically reads the file train.tsv
    <model1>             hole|rescal|transe|convkb
    <model2>             hole|rescal|transe|convkb
    <hash1>              The hash allowing to assert which settings were used during training for model 1
    <hash1>              The hash allowing to assert which settings were used during training for model 2
    <profile>           [default: default] AMIE's execution profile
Options:
    --log-file=<path>   If provided, the logs will (also) be written in this file.
    --quiet -q          Do not print anything in STDOUT (to keep logs, please use the --log-file option)
                        Please note that Critical/Error levels will still be outputted to STDERR.
    --verbose -v        Display more or less information (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help           Show this screen.
"""
import pandas as pd

from src.utils.disk_io import root_folder
from src.utils.output import compare_rules

if __name__ == '__main__':
    from docopt import docopt

    from utils.logger import configure_logger

    arguments = docopt(__doc__)
    logger = configure_logger(arguments['--verbose'], arguments['--log-file'], arguments['--quiet'])
    folder1 = f'{root_folder()}/data/results/{arguments["<profile>"]}/{arguments["<model1>"]}/{arguments["<dataset>"]}/{arguments["<hash1>"]}'
    folder2 = f'{root_folder()}/data/results/{arguments["<profile>"]}/{arguments["<model2>"]}/{arguments["<dataset>"]}/{arguments["<hash2>"]}'

    file1 = folder1 + '/all_in_one_summary.csv'
    file2 = folder2 + '/all_in_one_summary.csv'

    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)

    df_compare = compare_rules(df1, df2)
    df_compare.to_csv(f'{root_folder()}/data/results/{arguments["<profile>"]}/rules_comparison_{arguments["<model1>"]}_vs_{arguments["<model2>"]}_{arguments["<dataset>"]}.csv',
                      index=False)