from typing import Dict

from torchkge.exceptions import NoPreTrainedVersionError
from torchkge.utils.pretrained_models import load_pretrained_complex, load_pretrained_transe

from src.utils.disk_io import store_model, load_torchkge_knowledge_graph
from src.utils.evaluation import evaluate_bb_model
from src.utils.logger import get_logger

logger = get_logger()


class PreTrained:
    def __init__(self, model: str, kg_name: str):
        logger.debug(f'Retrieving pretrained model {model} for dataset {kg_name}')
        self.model_name = model
        self.kg_name = kg_name

        # store names, just in case
        self.model_name = model
        self.kg_name = kg_name

        if self.model_name == 'complex':
            load_model = load_pretrained_complex
        elif self.model_name == 'transe':
            load_model = load_pretrained_transe
        else:
            msg = f'There is no pretrained version for the model {self.model_name}.'
            logger.error(msg)
            raise NoPreTrainedVersionError(msg)

        settings = torch_kge_settings()
        if self.kg_name not in settings[self.model_name].keys():
            msg = f'The model {self.model_name} was not pretrained with the kg {self.kg_name}'
            logger.error(msg)
            raise NoPreTrainedVersionError(msg)

        self.model = load_model(self.kg_name, settings[self.model_name][self.kg_name])

    def evaluate(self):
        logger.debug('Launching pretrained evaluation')
        _, kg_val, kg_test = load_torchkge_knowledge_graph(self.kg_name)

        evaluation = evaluate_bb_model(self.model, kg_test, kg_val)

        associated_data = {
            'model': self.model_name,
            'kg': self.kg_name,
            'hash': 'pretrained',
            'evaluation': evaluation
        }

        store_model(self.model, associated_data)


def torch_kge_settings() -> Dict[str, Dict[str, int]]:
    """
    Settings as they can be found in the torchKGE library (`torchkge.utils.pretrained_models`)

    The integer value represent the embedding dimension.
    :return: The settings
    """
    return {
        'complex': {
            'wn18rr': 200,
            'fb15k237': 200,
            'wdv5': 200
        },
        'transe': {
            'fb15k': 100,
            'wn18rr': 100,
            'fb15k237': 150,
            'wdv5': 150,
            'yago310': 200
        }
    }
