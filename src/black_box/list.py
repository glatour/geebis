import json
import os
from os.path import abspath, join

from src.utils.disk_io import root_folder


def colored(r: int, g: int, b: int, text: str) -> str:
    """
    Return a string that will be colored when printed in the terminal

    :param r: The red value (0 -> 255)
    :param g: The green value (0 -> 255)
    :param b: The blue value (0 -> 255)
    :param text: The text that needs to be colored
    :return: The colored string
    """
    return '\033[38;2;{};{};{}m{} \033[38;2;255;255;255m'.format(r, g, b, text)


def available_combinations() -> None:
    """
    Display combination of models and knowledge graph that can be trained with this library.
    :return: Nothing but print information
    """
    models_folder = abspath(join(root_folder(), f'data/models'))
    models_calculated = {
        model: [file.split('_', 1)[0] for file in os.listdir(join(models_folder, model)) if file.endswith('.json')] for
        model in os.listdir(models_folder)}

    model_settings_path = abspath(join(root_folder(), f'data/settings/models.json'))
    data: dict = {}
    with open(model_settings_path, 'r') as setting_file:
        data = json.load(setting_file)

    print('')
    print('# Here are the available models and knowledge graph for training.')
    print('# If you want to train others, please update the `data/settings/models.json` file.')
    print('#')
    print('# > The combination of model & KG that are already trained are surrounded by *')
    for model, kgs in data.items():
        is_model_folder_created = model in models_calculated.keys()
        for kg in sorted(kgs):
            is_combination_calculated = False if not is_model_folder_created else kg in models_calculated[model]
            line = f'{model} {kg}'
            line = colored(142, 148, 242, f'* {line} *') if is_combination_calculated else line
            print(line)
