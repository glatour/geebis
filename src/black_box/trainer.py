import hashlib
import json
import pickle
import time
from os import path
from typing import Optional

from torch.optim import Adam, SGD
from torchkge import MarginLoss, KnowledgeGraph, LogisticLoss, ConvKBModel
from torchkge.data_structures import SmallKG
from torchkge.models import RESCALModel, TransEModel, HolEModel, ComplExModel

from src.utils.disk_io import load_knowledge_graph, root_folder, store_model
from src.utils.evaluation import evaluate_bb_model
from src.utils.logger import get_logger
from src.utils.settings import get_model_setting, get_setting
from src.utils.torchkge.kg import kg2df
from src.utils.torchkge.training import CustomTrainer

logger = get_logger()


def display_available_training() -> None:
    pass


class ModelTrainer:
    """
    Wrapper to train a certain model on a certain knowledge graph.

    Of course the model and graph must be supported in order to work properly.
    You can check the available combination with the command `train list`.

    The meta-parameters for the models are stored in the settings folder `data/settings/models.json`.
    """

    def __init__(self, model: str, kg_name: str):
        """
        Init the wrapper.
        Both parameters need to be valid.
        Please check the `available_combinations` function to know which combinations are valid.

        :param model: The name of the model
        :param kg_name: The name of the dataset (knowledge graph)
        """
        logger.debug(f'Creating a trainer for model {model} -> {kg_name}')

        # store names, just in case
        self.model_name = model
        self.kg_name = kg_name

        # load settings and knowledge graph
        self.settings = get_model_setting(model, kg_name)
        self.df, self.kg = load_knowledge_graph(kg_name)

        self.hash = hashlib.sha256(json.dumps({'name': model, 'kg': kg_name, **self.settings}).encode()).hexdigest()
        # the model would be stored in this file
        self.model_file_path = path.join(root_folder(), f'data/models/{self.model_name}/{self.kg_name}_{self.hash}.pkl')

        # check if the trained model file exist and load that model if it does
        self.is_trained = path.isfile(self.model_file_path)
        if self.is_trained:
            with open(self.model_file_path, 'rb') as pickle_file:
                self.model = pickle.load(pickle_file)
            logger.info('Already trained model is loaded.')
        else:
            self.model = None

    def train(self, skip_eval=False, full_eval=True):
        """
        This is where the magic happens.

        With the given parameters (configurable via json settings), the model will be trained with the training set
        of the knowledge graph.

        This function uses TorchKGE trainer which is already a wrapper.

        At the end, two files will be generated: the model and information about the model.
        Both files have the same name and differ by the extension (.pkl and .json)
        """
        logger.info(f'Training model {self.model_name} for KG {self.kg_name}.')
        ping = time.time()

        # instantiate the model
        if self.model_name == 'rescal':
            self.model = RESCALModel(self.settings['embedding_dimension'],
                                     self.kg.n_ent,
                                     self.kg.n_rel)

        elif self.model_name == 'transe':
            self.model = TransEModel(self.settings['embedding_dimension'],
                                     self.kg.n_ent,
                                     self.kg.n_rel)

        elif self.model_name == 'hole':
            self.model = HolEModel(self.settings['embedding_dimension'],
                                   self.kg.n_ent,
                                   self.kg.n_rel)
        elif self.model_name == 'complex':
            self.model = ComplExModel(self.settings['embedding_dimension'],
                                      self.kg.n_ent,
                                      self.kg.n_rel)
        # todo : implement convKB model option.
        # To do so, the setting of each convkb dataset must have a n_filters value
        # This is commented out right now because more research must be made to understand what this parameter do
        elif self.model_name == 'convkb':
             self.model = ConvKBModel(self.settings['embedding_dimension'],
                                      self.settings['n_filters'],
                                      self.kg.n_ent,
                                      self.kg.n_rel)
        else:
            msg = f'Model {self.model_name} was not found!'
            logger.error(msg)
            raise Exception(msg)

        # Set up criterion (objective function)
        if self.settings['objective_function'] == 'logistic':
            criterion = LogisticLoss()
        elif self.settings['objective_function'] == 'margin':
            criterion = MarginLoss(self.settings['margin'])
        else:
            msg = f'Objective function ({self.settings["objective_function"]}) not found!'
            logger.error(msg)
            raise Exception(msg)

        # Set up optimizer
        if self.settings['optimizer'].lower() == 'sgd':
            optimizer = SGD(self.model.parameters(), self.settings['learning_rate'])
        elif self.settings['optimizer'].lower() == 'adam':
            optimizer = Adam(self.model.parameters(), self.settings['learning_rate'], weight_decay=1e-5)
        else:
            msg = f'Optimizer ({self.settings["optimizer"]}) not found!'
            logger.error(msg)
            raise Exception(msg)

        # allow gpu usage if the setting says so (data/settings/global.json)
        use_cuda = 'all' if get_setting('use_cuda') else None

        trainer = CustomTrainer(self.model,
                                criterion,
                                self.kg,
                                self.settings['n_epochs'],
                                self.settings['batch_size'],
                                optimizer=optimizer,
                                sampling_type=get_setting('negative_sampling'),
                                use_cuda=use_cuda)
        trainer.run()

        counter_examples: Optional[SmallKG] = trainer.get_counter_examples()
        if counter_examples is None:
            msg = "No counter examples generated."
            logger.error(msg)
            raise Exception(msg)
        counter_examples: KnowledgeGraph = KnowledgeGraph(kg={'heads': counter_examples.head_idx,
                                                              'relations': counter_examples.relations,
                                                              'tails': counter_examples.tail_idx
                                                              },
                                                          ent2ix=self.kg.ent2ix,
                                                          rel2ix=self.kg.rel2ix)

        test_df, test_kg = load_knowledge_graph(self.kg_name, False)
        test_kg = KnowledgeGraph(df=test_df.loc[test_df['is_true']][['from', 'rel', 'to']])
        if not skip_eval :
            try :
                evaluation = evaluate_bb_model(self.model, self.kg, test_kg, full_eval=full_eval)
            except Exception as e:
                print(e)
                evaluation = {}
        else:
            evaluation = {}

        associated_data = {
            'model': self.model_name,
            'kg': self.kg_name,
            'hash': self.hash,
            'settings': self.settings,
            'evaluation': evaluation
        }

        logger.debug("Storing model")
        store_model(self.model, associated_data, kg2df(counter_examples))

        pong = time.time()
        logger.info(f'The training took {pong - ping:.2f}s')
