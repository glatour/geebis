#!/usr/bin/python3

"""
Gee Synthetize

Usage:
    synthetize
"""
import os
import sys

import numpy as np
import pandas as pd

from src.utils.output import synthetize_results, compare_synthetized_results, latex_header, latex_footer

root_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, root_directory)
# end of ugly hack

from src.utils.disk_io import root_folder, load_knowledge_graph

models = ['complex', 'transe', 'hole', 'convkb']
datasets = ['fb15k237', 'wn18rr', 'yago3-10']
#models = ['convkb']
#datasets = ['yago3-10']
hashes = {('wn18rr', 'complex'): 'pretrained',
          ('fb15k237', 'complex'): 'pretrained',
          ('yago3-10', 'complex'): '3997a9c047ee1ce2fa9ab991a3fb39934e3279696acd3447c98ecdfacae2e29a',
          ('wn18rr', 'transe'): 'pretrained',
          ('fb15k237', 'transe'): 'pretrained',
          ('yago3-10', 'transe'): '1e3514dbdc7462f4a93e1d4748dd1129f080d3e153f84bcdc2cf0356cb8e8249',
          ('wn18rr', 'convkb'): 'ba9b2bc5d71cf5aeae0e8348a211a3ade00ed62e4f2f6a873b364a19a5cb0a32',
          ('fb15k237', 'convkb'): '0fbcade8e60673766a97b77fef5adf156947ddd26cbc53014cf1de395d1719fa',
          ('yago3-10', 'convkb'): '0d8f749aebdeecebf506652719cbedc3f074838886450a3008111c73f0cc779d',
          ('wn18rr', 'hole'): 'c48074e2f8d19a454d89bdc28a6eea379365c1ee6b3cef5284a1aeea053bb750',
          ('fb15k237', 'hole'): '8405323af2dd26e31babafc7adb3f8154020dfc9f794a767139b2d34b8c52684',
          ('yago3-10', 'hole'): 'e96fe144bfb7ad5eb2dbc61a9644f9ea91e9b9c2db2beee32acdacb4297295ac'
          }


def extract_scores(results: pd.DataFrame):
    score_local = {}
    score_cluster = {}
    score_global = {}

    score_global['roc-auc'] = '-'
    score_global['predicate-coverage'] = '-'
    score_global['s-mrr'] = '-'
    score_global['o-mrr'] = '-'

    score_cluster['roc-auc'] = '-'
    score_cluster['predicate-coverage'] = '-'
    score_cluster['s-mrr'] = '-'
    score_cluster['o-mrr'] = '-'

    score_local['roc-auc'] = '-'
    score_local['predicate-coverage'] = '-'
    score_local['s-mrr'] = '-'
    score_local['o-mrr'] = '-'

    if len(results) > 0:
        not_trivial_results = results[results['not-trivial-global'] == 1]
        if len(not_trivial_results) > 0:
            roc_auc_mask = ~(not_trivial_results['roc-auc-global'].isnull())
            if roc_auc_mask.any():
                score_global['roc-auc'] = \
                    np.average(not_trivial_results.loc[roc_auc_mask]['roc-auc-global'],
                               weights=not_trivial_results.loc[roc_auc_mask]['predicate-coverage-in-valid-and-test'])
            else:
                score_global['roc-auc'] = float('nan')
            score_global['predicate-coverage'] = \
                np.sum(not_trivial_results.loc[roc_auc_mask]['predicate-coverage-in-valid-and-test'])
            s_mrr_mask = ~(not_trivial_results['subject-mrr-global'].isnull())
            score_global['s-mrr'] = \
                np.average(not_trivial_results.loc[s_mrr_mask]['subject-mrr-global'],
                           weights=not_trivial_results.loc[s_mrr_mask]['predicate-coverage-in-valid-and-test'])
            o_mrr_mask = ~(not_trivial_results['object-mrr-global'].isnull())
            score_global['o-mrr'] = \
                np.average(not_trivial_results.loc[o_mrr_mask]['object-mrr-global'],
                           weights=not_trivial_results.loc[o_mrr_mask]['predicate-coverage-in-valid-and-test'])

        not_trivial_results = results[results['not-trivial-cluster'] > 0]
        if len(not_trivial_results) > 0:
            roc_auc_mask = ~(not_trivial_results['roc-auc-cluster'].isnull())
            if roc_auc_mask.any():
                score_cluster['roc-auc'] = \
                    np.average(not_trivial_results.loc[roc_auc_mask]['roc-auc-cluster'],
                               weights=not_trivial_results.loc[roc_auc_mask]['support-cluster'])
            else:
                score_cluster['roc-auc'] = float('nan')
            score_cluster['predicate-coverage'] = \
                np.sum((not_trivial_results.loc[roc_auc_mask]['support-cluster'] * (10/6) /
                        (not_trivial_results.loc[roc_auc_mask]['predicate-cardinality-in-valid'] +
                         not_trivial_results.loc[roc_auc_mask]['predicate-cardinality-in-test']))
                       * not_trivial_results.loc[roc_auc_mask]['predicate-coverage-in-valid-and-test'])
            s_mrr_mask = ~(not_trivial_results['subject-mrr-cluster'].isnull())
            if s_mrr_mask.any():
                score_cluster['s-mrr'] = \
                    np.average(not_trivial_results.loc[s_mrr_mask]['subject-mrr-cluster'],
                               weights=not_trivial_results.loc[s_mrr_mask]['predicate-coverage-in-valid-and-test'])
            else:
                score_cluster['s-mrr'] = float('nan')
            o_mrr_mask = ~(not_trivial_results['object-mrr-cluster'].isnull())
            if o_mrr_mask.any():
                score_cluster['o-mrr'] = \
                    np.average(not_trivial_results.loc[o_mrr_mask]['object-mrr-cluster'],
                               weights=not_trivial_results.loc[o_mrr_mask]['predicate-coverage-in-valid-and-test'])
            else:
                score_cluster['o-mrr'] = float('nan')
        not_trivial_results = results[results['not-trivial-local'] > 0]
        if len(not_trivial_results) > 0:
            roc_auc_mask = ~(not_trivial_results['roc-auc-local'].isnull())
            if roc_auc_mask.any():
                score_local['roc-auc'] = \
                    np.average(not_trivial_results.loc[roc_auc_mask]['roc-auc-local'],
                               weights=not_trivial_results.loc[roc_auc_mask]['predicate-coverage-in-test'])
            coverage_mask = ~(not_trivial_results['support-local'].isnull())
            if coverage_mask.any():
                score_local['predicate-coverage'] = \
                    np.sum((not_trivial_results.loc[coverage_mask]['support-local'] / \
                    not_trivial_results.loc[coverage_mask]['predicate-cardinality-in-test']) *
                           not_trivial_results.loc[coverage_mask]['predicate-coverage-in-test'])

            s_mrr_mask = ~(not_trivial_results['subject-mrr-local'].isnull())
            if s_mrr_mask.any():
                score_local['s-mrr'] = \
                    np.average(not_trivial_results.loc[s_mrr_mask]['subject-mrr-local'],
                               weights=not_trivial_results.loc[s_mrr_mask]['predicate-coverage-in-test'])
            o_mrr_mask = ~(not_trivial_results['object-mrr-local'].isnull())
            if o_mrr_mask.any():
                score_local['o-mrr'] = \
                    np.average(not_trivial_results.loc[o_mrr_mask]['object-mrr-local'],
                               weights=not_trivial_results.loc[o_mrr_mask]['predicate-coverage-in-test'])

    return score_local, score_cluster, score_global


def output_latex(summary, metrics=['ROC-AUC', 'S-MRR', 'O-MRR', 'Coverage']):
    output = latex_header(metrics) + '\n'
    for model in models:
        vals_for_model = []
        for metric in metrics:
            for rule_type in ['noconst', 'default']:
                for broadness in ['local', 'cluster', 'global']:
                    key = model+dataset+broadness+rule_type+metric.lower()
                    if key in summary:
                        vals_for_model.append(summary[key])
                    else:
                        vals_for_model.append('--')

        output += model + '&' + '&'.join(["{:.2f}".format(x if isinstance(x, float) else 0.0) for x in vals_for_model]) + '\\\\ \n'

    return output + latex_footer()


if __name__ == '__main__':
    from docopt import docopt
    from utils.logger import configure_logger

    arguments = docopt(__doc__)
    logger = configure_logger(True, None, True)

    summary = {}

    for dataset in datasets:
        for model in models:
            hash = hashes[(dataset, model)]
            file_noconst = f'{root_folder()}/data/results/noconst/{model}/{dataset}/{hash}/all_in_one_summary.csv'
            results_noconst = None
            results_default = None
            if os.path.exists(file_noconst):
                results_noconst = synthetize_results(file_noconst, dataset_name=dataset)
                score_local, score_cluster, score_global = extract_scores(results_noconst)

                summary[model + dataset + 'globalnoconstroc-auc'] = score_global['roc-auc']
                summary[model + dataset + 'globalnoconsts-mrr'] = score_global['s-mrr']
                summary[model + dataset + 'globalnoconsto-mrr'] = score_global['o-mrr']
                summary[model + dataset + 'globalnoconstcoverage'] = score_global['predicate-coverage']

                summary[model + dataset + 'localnoconstroc-auc'] = score_local['roc-auc']
                summary[model + dataset + 'localnoconsts-mrr'] = score_local['s-mrr']
                summary[model + dataset + 'localnoconsto-mrr'] = score_local['o-mrr']
                summary[model + dataset + 'localnoconstcoverage'] = score_local['predicate-coverage']

                summary[model + dataset + 'clusternoconstroc-auc'] = score_cluster['roc-auc']
                summary[model + dataset + 'clusternoconsts-mrr'] = score_cluster['s-mrr']
                summary[model + dataset + 'clusternoconsto-mrr'] = score_cluster['o-mrr']
                summary[model + dataset + 'clusternoconstcoverage'] = score_cluster['predicate-coverage']

            hash = hashes[(dataset, model)]
            file_default = f'{root_folder()}/data/results/default/{model}/{dataset}/{hash}/all_in_one_summary.csv'
            if os.path.exists(file_default):
                results_default = synthetize_results(file_default, dataset_name=dataset)
                score_local, score_cluster, score_global = extract_scores(results_default)

                summary[model+dataset+'globaldefaultroc-auc'] = score_global['roc-auc']
                summary[model+dataset+'globaldefaults-mrr'] = score_global['s-mrr']
                summary[model+dataset+'globaldefaulto-mrr'] = score_global['o-mrr']
                summary[model+dataset+'globaldefaultcoverage'] = score_global['predicate-coverage']

                summary[model + dataset + 'localdefaultroc-auc'] = score_local['roc-auc']
                summary[model + dataset + 'localdefaults-mrr'] = score_local['s-mrr']
                summary[model + dataset + 'localdefaulto-mrr'] = score_local['o-mrr']
                summary[model + dataset + 'localdefaultcoverage'] = score_local['predicate-coverage']

                summary[model + dataset + 'clusterdefaultroc-auc'] = score_cluster['roc-auc']
                summary[model + dataset + 'clusterdefaults-mrr'] = score_cluster['s-mrr']
                summary[model + dataset + 'clusterdefaulto-mrr'] = score_cluster['o-mrr']
                summary[model + dataset + 'clusterdefaultcoverage'] = score_cluster['predicate-coverage']

            results = compare_synthetized_results(results_noconst, results_default)
            results.to_csv(f'{root_folder()}/data/results/{dataset}_{model}_all_in_one_summary.csv', index=False)
            ## Output subfiles per metric
            results_of_interest = results[
                (results['not-trivial-global_default'] == 1) |
                (results['not-trivial-global_noconst'] == 1) |
                (results['not-trivial-local_default'] == 1) |
                (results['not-trivial-local_noconst'] == 1)
            ]
            for metric in ['accuracy', 'roc-auc', 'subject-mrr', 'object-mrr', 'predicate-coverage-in-valid-and-test']:
                results_metric = results_of_interest[[col for col in results.columns if col.startswith(metric)] +
                                                     ['predicate', 'bb-accuracy-global_default',
                                                      'bb-accuracy-global_noconst', 'bb-accuracy-local_default',
                                                      'bb-accuracy-local_noconst']]
                results_metric.to_csv(f'{root_folder()}/data/results/{dataset}_{model}_{metric}_all_in_one_summary.csv',
                                   index=False)

        latex_table = output_latex(summary, metrics=['ROC-AUC', 'S-MRR', 'O-MRR'])
        outf = open(f'{root_folder()}/data/results/{dataset}.tex', 'w')
        outf.write(latex_table)
        outf.close()