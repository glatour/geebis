import json
from glob import glob
from os.path import basename

import pandas as pd
from tqdm import tqdm

from src.utils.disk_io import root_folder
from src.utils.logger import get_logger

logger = get_logger()


def parse_results(profile: str, model_name: str, kg_name: str, hash_name: str) -> pd.DataFrame:
    """
    Loop through all the files in `results/<profile>/<model>/<dataset>/<hash>` and compute a report.

    :param profile
    :param model_name:
    :param kg_name:
    :param hash_name:
    :return:
    """
    folder = f'{root_folder()}/data/results/{profile}/{model_name}/{kg_name}/{hash_name}'
    reports = []
    for file_name in tqdm([f for f in glob(f'{folder}/*') if f.endswith('.json')], desc="file"):
        report = {}
        if 'metrics_local_' in file_name:
            report['broadness'] = 'local'
        elif 'metrics_cluster_' in file_name:
            report['broadness'] = 'cluster'
        else:
            report['broadness'] = 'global'
        report['id'] = basename(file_name).replace(f'metrics_{report["broadness"]}_', '').replace('.json', '')
        rules_file, metrics_file = file_name.replace('.json', '.csv').replace('metrics', 'rules'), file_name

        with open(metrics_file, 'r') as metrics_handle:
            metrics = json.load(metrics_handle)

        # extract triplet classification metrics
        report['context_accuracy'] = metrics['triplet_classification']['context_accuracy']
        report['highbrow_precision'] = metrics['triplet_classification']['highbrow_precision']
        report['black_box_accuracy'] = metrics['triplet_classification']['black_box_accuracy']
        report['context_roc_auc'] = metrics['triplet_classification']['context_roc_auc']
        report['support'] = metrics['triplet_classification']['support']
        report['highbrow_positive_predictions'] = metrics['triplet_classification']['highbrow_positive_predictions']

        # extract link prediction metrics
        if metrics['link_prediction']['subject'] is not None and metrics['link_prediction']['object'] is not None:
            subject_mrr = metrics['link_prediction']['subject']['mean_reciprocal_rank']
            subject_support = sum(metrics['link_prediction']['subject']['support'])
            object_mrr = metrics['link_prediction']['object']['mean_reciprocal_rank']
            object_support = sum(metrics['link_prediction']['object']['support'])
            subject_coefficient = 1
            object_coefficient = object_support / subject_support
            report['agg-mrr'] = ((subject_mrr / subject_support * subject_coefficient) +
                             (object_mrr / object_support * object_coefficient)) / (
                                    subject_coefficient + object_coefficient)
        if metrics['link_prediction']['subject'] is not None:
            report['s-mrr'] = metrics['link_prediction']['subject']['mean_reciprocal_rank']
            report['s-mr'] = metrics['link_prediction']['subject']['mean_rank']
        if metrics['link_prediction']['object'] is not None:
            report['o-mrr'] = metrics['link_prediction']['object']['mean_reciprocal_rank']
            report['o-mr'] = metrics['link_prediction']['object']['mean_rank']
        else:
            report['s-mrr'] = None
            report['s-mr'] = None
            report['o-mrr'] = None
            report['o-mr'] = None
            report['agg-mrr'] = None

        # extract the top 10 of the rules used
        rules = pd.read_csv(rules_file)
        keys = list(rules.columns)
        sum_of_rules = [(k, rules[k].sum()) for k in keys]
        top_rules = [rule for rule, score in sorted(sum_of_rules, key=lambda r: r[1], reverse=True) if
                     score > 0.0][:10]
        for i in range(10):
            report[f'rule {i + 1}'] = '' if i >= len(top_rules) else top_rules[i]

        reports.append(report)
    return pd.DataFrame(reports)
