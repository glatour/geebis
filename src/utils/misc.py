import random
import string
from typing import List


def get_random_string(length: int, collisions: List[str] = None) -> str:
    """
    Generate a random string with the combination of lower and upper case

    :param length: The number of letters that the string have to contain.
    :param collisions: An array of words that will be avoided.
    :return: The generated string.
    :rtype: str
    """
    collisions = collisions if collisions is not None else []
    letters = string.ascii_letters

    def generation(n: int):
        return ''.join([random.choice(letters) for _ in range(length)])

    w = generation(length)
    while w in collisions:
        w = generation(length)

    return w


def negate_predicate(predicate: str) -> str:
    """
    Annotate the predicate in a way to propose a negation of that predicate.

    :param predicate: The predicate string that need to be negated.
    :return: A negated predicate.
    """
    is_prefixed = predicate.startswith('__')
    predicate = predicate[2:] if is_prefixed else predicate
    predicate = predicate.replace('<', '<neg_', 1) if predicate.startswith('<') else 'neg_' + predicate
    return f'__{predicate}' if is_prefixed else predicate
