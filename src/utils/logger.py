import logging
import os
from os import path

gee_logger = None
DEFAULT_LOG_PATH = path.join(path.dirname(path.dirname(path.dirname(__file__))), 'data/logs/gee.log')


def configure_logger(verbose_level: int, log_file: os.path = None, quiet: bool = False) -> logging.Logger:
    """
    Configure a logger with the name 'gee-logger' and according to the provided parameters.

    :param int verbose_level: The value of verbosity will impact logging level
        - 0 : critical & error
        - 1 : warning
        - 2 : info
        - 3 : debug
    :param str log_file: The file where the log should be stored (if any);
        If no file is provided, then the log will (obviously) not be written in a file.
    :param bool quiet: True to prevent outputting something in STDOUT.
    :return: The configured logger.
    :rtype: logging.Logger
    """
    logger = logging.getLogger('gee-logger')

    # prevent automatic handlers to send messages to STDOUT
    for handler in logger.handlers.copy():
        logger.removeHandler(handler)
    logger.addHandler(logging.NullHandler())
    logger.propagate = False

    level = logging.ERROR
    if verbose_level == 1:
        level = logging.WARNING
    elif verbose_level == 2:
        level = logging.INFO
    elif verbose_level > 2:
        level = logging.DEBUG

    logger.setLevel(level)

    # Stream handler
    if not quiet:
        c_handler = logging.StreamHandler()
        c_handler.setFormatter(logging.Formatter('%(levelname)s - %(message)s'))
        c_handler.setLevel(level)
        logger.addHandler(c_handler)

    # File handler
    if log_file is not None:
        log_file = DEFAULT_LOG_PATH if log_file == 'default' else log_file
        f_handler = logging.FileHandler(log_file)
        f_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        f_handler.setLevel(level)
        logger.addHandler(f_handler)

    # Since it's likely that this function will only be called once in an execution,
    # and since it should be the first thing to do in each execution,
    # it seems to be a good place to display this NEW RUN string.
    logger.info('------===== NEW RUN =====------')

    return logger


def get_logger() -> logging.Logger:
    """
    Get the `gee-logger` logger !

    :return: The `gee-logger`
    :rtype: logging.Logger
    """
    global gee_logger
    if gee_logger is None:
        gee_logger = configure_logger(0)

    return gee_logger
