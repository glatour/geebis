from typing import Dict, List, Tuple, Any

import numpy
import pandas as pd
from sklearn.metrics import mean_squared_error, r2_score, accuracy_score, log_loss, roc_auc_score, precision_score
from torchkge import LinkPredictionEvaluator, KnowledgeGraph
from torchkge.models import Model

from src.utils.logger import get_logger
from src.utils.torchkge.evaluation.triplet_classification import PredicateTripletClassificationEvaluator

logger = get_logger()


def evaluate_bb_model(model: Model, kg_test: KnowledgeGraph, kg_val: KnowledgeGraph = None, full_eval=True) -> Dict:
    """
    Evaluate the given model through 2 tasks: link prediction and triplet classification.

    The link prediction task give the following metrics: hit@5, hit@10, mean rank and mrr.
    The triplet classification task give the accuracy and the accuracy by predicate.

    If there is no validation set, the test set will be used instead.

    :param model: The model you want to evaluate
    :param kg_test: The testing set
    :param kg_val: The validation set
    :param full_eval: If true the function executes a link prediction and triple classification evaluation. If false
    the triple classification is omitted.
    :return: A bunch of metrics described above.
    """
    # the validation set is only used during the triplet classification task, for generating counter examples.
    kg_val = kg_test if kg_val is None else kg_val

    batch_size = 64

    logger.debug("Link prediction evaluation")
    link_prediction_evaluator = LinkPredictionEvaluator(model, kg_test)
    link_prediction_evaluator.evaluate(batch_size)

    logger.debug("Triplet classification evaluation")
    triplet_classification_evaluator = PredicateTripletClassificationEvaluator(model, kg_val, kg_test)

    if full_eval:
        accuracy_global, accuracy_by_predicate, roc_auc_score, roc_auc_score_by_predicate \
            = triplet_classification_evaluator.accuracy(batch_size)
    else :
        accuracy_global = roc_auc_score = float('nan')
        accuracy_by_predicate = roc_auc_score_by_predicate = {}

    return {
        'hit@5': link_prediction_evaluator.hit_at_k(5),
        'hit@10': link_prediction_evaluator.hit_at_k(10),
        'mean_rank': link_prediction_evaluator.mean_rank(),
        'mrr': link_prediction_evaluator.mrr(),
        'accuracy': accuracy_global,
        'accuracy_by_predicate': accuracy_by_predicate,
        'roc_auc' : roc_auc_score,
        'roc_auc_by_predicate': roc_auc_score_by_predicate
    }


def link_prediction_evaluation(highbrow_predicted: numpy.ndarray,
                               real_df: pd.DataFrame) -> Dict[str, Dict]:
    """
    Evaluation of the link prediction task.

    In the given context (real_df), every true triplet (seen as true by the black box model) will be used to generate
    a list of (subject, object).
    For each tuple subject-object in this list, a sublist containing all the fact in the context having the same
    subject will be generated.
    We then can order this (sub)list by score and rank where the object is.

    The same principle is applied to rank the subjects.

    Note that we don't care about the predicate because in the context making, we only keep identical predicate.

    :param highbrow_predicted: The scores of the highbrow model.
    :param real_df: The facts and their score through the black-box model.
    :param thresholds: if the score is superior to its threshold, the fact is considered to be true.
    :return: A dictionary of the metrics. Both 'subject' and 'object'
    """

    def get_ranks(tab: List[Tuple[str, str]], df: pd.DataFrame, which: str = 'subject') -> Dict[str, Any]:
        """
        Calculate ranking metrics (rank, mean reciprocal rank, mean rank) from a list of tuple (subject, object) and a
        dataframe containing at least the following columns: ['from', 'to', 'score'].

        :param tab: The list of tuple subject, object
        :param df: The dataframe containing at least the columns ['from', 'to', 'score'].
        :param which: On which element of the tuple is made the prediction.
                      If the prediction is made on 'subject', it means that for each tuple in `tab`,
                      an array containing all the different subjects (for the same object)
                      will be generated and sorted by their score (DESC) to compute their rank
        :return: A dictionary containing the ranks as they are calculated, the support of those ranks and
                 2 useful metrics : mean reciprocal rank and mean rank
        """
        if which not in ['subject', 'object']:
            raise Exception('Which must be either "subject" or "object"')

        if len(tab) <= 0:
            logger.error('There is no element to make link prediction to.')
            return None

        target, compare = ('from', 'to') if which == 'subject' else ('to', 'from')

        ranks = []
        support = []
        for s, o in tab:
            target_to, compare_to = (s, o) if which == 'subject' else (o, s)

            # create all the targets (either subjects or objects) with the associated score
            tmp_tab = [(f[target], f['score']) for f in df.loc[df[compare] == compare_to].to_dict('records')]
            tmp_tab.sort(key=lambda x: x[1], reverse=True)
            rank = [i for i, _ in tmp_tab].index(target_to)
            ranks.append(rank + 1)
            support.append(len(tmp_tab))

        mean_reciprocal_rank = sum(map(lambda x: 1 / x, ranks)) / len(ranks)
        mean_rank = sum(ranks) / len(ranks)
        return {
            'ranks': ranks,
            'support': support,
            'mean_reciprocal_rank': mean_reciprocal_rank,
            'mean_rank': mean_rank
        }

    highbrow_df = pd.DataFrame({'from': real_df['from'],
                                'rel': real_df['rel'],
                                'to': real_df['to'],
                                'score': highbrow_predicted})

    # the truth mask is applied through the eye of the black box model
    truth_mask = real_df['binarized_score']

    # separate facts with same subject (for each subject in mask, get all facts)
    tuples = [(f['from'], f['to']) for f in real_df.loc[truth_mask, ['from', 'to']].to_dict('records')]
    subject_ranks = get_ranks(tuples, highbrow_df)

    # separate facts with same object (for each object in mask, get all facts)
    object_ranks = get_ranks(tuples, highbrow_df, 'object')

    return {
        'subject': subject_ranks,
        'object': object_ranks
    }


def triplet_classification_evaluation(highbrow_predicted: numpy.ndarray,
                                      black_box_predicted_df: pd.DataFrame) -> Dict[str, float]:
    """
    Compare the results of the highbrow prediction and the black-box prediction.

    :param highbrow_predicted: The scores predicted by the highbrow model.
    :param black_box_predicted_df: The df containing the facts and the black box model prediction.
    :return: Multiple metrics concerning the triplet classification task
    """
    assert (len(highbrow_predicted) == len(black_box_predicted_df))

    context_mean_squared_error = mean_squared_error(black_box_predicted_df['normalized_score'], highbrow_predicted)
    context_r2 = r2_score(black_box_predicted_df['normalized_score'], highbrow_predicted)

    highbrow_predicted_binarized = list(map(lambda x: x >= 0.5, highbrow_predicted))
    context_accuracy_score_norm = accuracy_score(black_box_predicted_df['binarized_score'],
                                                 highbrow_predicted_binarized)
    context_precision = precision_score(black_box_predicted_df['binarized_score'],
                                                 highbrow_predicted_binarized)
    if len(set(black_box_predicted_df['binarized_score'])) < 2:
        logger.warning('There is only one class present in y_true, no ROC AUC neither log loss will be computed.')
        context_log_loss_score_norm = None
        context_roc_auc_score_norm = None
    else:
        context_log_loss_score_norm = log_loss(black_box_predicted_df['binarized_score'], highbrow_predicted,
                                               labels=[True, False])
        context_roc_auc_score_norm = roc_auc_score(black_box_predicted_df['binarized_score'], highbrow_predicted,
                                                   labels=[True, False])

    correctly_bb_facts = [(row['from'], row['rel'], row['to']) for row in black_box_predicted_df.to_dict('records') if
                          row['is_true'] == row['binarized_score']]

    return {
        'context_mse': context_mean_squared_error,
        'context_r2': context_r2,
        'context_accuracy': context_accuracy_score_norm,
        'context_log_loss': context_log_loss_score_norm,
        'context_roc_auc': context_roc_auc_score_norm,
        'highbrow_precision': context_precision,
        'black_box_accuracy': len(correctly_bb_facts) / len(highbrow_predicted),
        'support': len(highbrow_predicted),
        'highbrow_positive_predictions': int(sum(highbrow_predicted_binarized))
    }
