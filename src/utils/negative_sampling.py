from torchkge import KnowledgeGraph
from torchkge.sampling import BernoulliNegativeSampler

from src.utils.settings import get_setting


def generate_counter_examples(training_kg: KnowledgeGraph,
                              validation_kg: KnowledgeGraph = None,
                              testing_kg: KnowledgeGraph = None,
                              which: str = 'main'
                              ) -> KnowledgeGraph:
    """
    Simple wrapper to generate counter examples from a KG
    Each triplet will generate a counter example.

    Of course, if you want to generate counter-examples around the testing set, you MUST give a non null testing_kg.

    :param training_kg: The training set of the KG
    :param validation_kg: The validation set of the KG
    :param testing_kg: The testing set of the KG
    :param which: which set have to be tweaked for the counter-examples generation
    :return KnowledgeGraph: A new knowledge graph with counter examples.
    """
    sampler = BernoulliNegativeSampler(training_kg, kg_val=validation_kg, kg_test=testing_kg, n_neg=1)
    sampler.corrupt_kg(64, get_setting('use_cuda'), which)
