import math
from typing import Dict, Tuple

import numpy as np
import torch
from sklearn.linear_model import LogisticRegressionCV, LogisticRegression
from sklearn.metrics import roc_auc_score
from torch import zeros
from torchkge import TripletClassificationEvaluator, KnowledgeGraph
from torchkge.models import Model
from torchkge.sampling import PositionalNegativeSampler

from src.utils.logger import get_logger
from src.utils.settings import get_setting

logger = get_logger()


class PredicateTripletClassificationEvaluator(TripletClassificationEvaluator):
    """
    This evaluator has the particularity to measure the accuracy by predicate.

    Apart from that, it's the exact replica of the `TripletClassificationEvaluator`
    """

    def __init__(self, model: Model, kg_val: KnowledgeGraph, kg_test: KnowledgeGraph):
        super().__init__(model, kg_val, kg_test)

    def accuracy(self, b_size: int) -> Tuple[float, Dict[str, float], float, Dict[str, float]]:
        """
        The return value is a dictionary having the names of the predicate for the key and its value is the accuracy.

        :param b_size: The size of the batch. Only for performance.
        :return: A tuple containing
            * the global accuracy
            * a dictionary of each predicate and their specific accuracy
            * the global roc auc score
            * a dictionary of each predicate and their specific accuracy
        """
        if not self.evaluated:
            self.evaluate(b_size)

        r_idx = self.kg_test.relations

        neg_heads, neg_tails = self.sampler.corrupt_kg(b_size,
                                                       self.is_cuda,
                                                       which='test')
        original_scores = self.get_scores(self.kg_test.head_idx,
                                 self.kg_test.tail_idx,
                                 r_idx,
                                 b_size)
        original_neg_scores = self.get_scores(neg_heads, neg_tails, r_idx, b_size)

        if self.is_cuda:
            self.thresholds = self.thresholds.cuda()

        scores = (original_scores > self.thresholds[r_idx])
        neg_scores = (original_neg_scores < self.thresholds[r_idx])

        # first calculate global accuracy
        accuracy = (scores.sum().item() + neg_scores.sum().item()) / (2 * self.kg_test.n_facts)
        y_score = np.concatenate([original_scores.detach().numpy(), original_neg_scores.detach().numpy()])
        y_true = np.concatenate([
            np.ones(len(original_scores)), np.zeros(len(original_neg_scores))
        ])
        roc_auc = roc_auc_score(y_true, y_score)

        # then calculate each predicate specific accuracy
        accuracy_by_predicate = {}
        aucroc_by_predicate = {}
        for predicate, rel_index in self.kg_test.rel2ix.items():
            mask = (self.kg_test.relations == rel_index)
            # prevent DivideByZeroException
            if torch.count_nonzero(mask).item() == 0:
                logger.warning(f'There is zero relation {predicate} and that\'s weird')
                continue
            original_masked_scores = torch.masked_select(original_scores, mask)
            original_masked_neg_scores = torch.masked_select(original_neg_scores, mask)
            masked_scores = torch.masked_select(scores, mask)
            masked_neg_scores = torch.masked_select(neg_scores, mask)
            accuracy_by_predicate[predicate] = (
                                                       torch.count_nonzero(masked_scores).item() +
                                                       torch.count_nonzero(masked_neg_scores).item()
                                               ) / (2 * torch.count_nonzero(mask).item())
            y_score = np.concatenate([original_masked_scores.detach().numpy(),
                                      original_masked_neg_scores.detach().numpy()])
            y_true = np.concatenate([
                np.ones(len(original_masked_scores)), np.zeros(len(original_masked_neg_scores))
            ])
            aucroc_by_predicate[predicate] = roc_auc_score(y_true, y_score)

        # sort the predicate from the most accurate to the least
        accuracy_by_predicate = dict(sorted(accuracy_by_predicate.items(), key=lambda item: item[1], reverse=True))
        aucroc_by_predicate = dict(sorted(aucroc_by_predicate.items(), key=lambda item: item[1], reverse=True))

        return accuracy, accuracy_by_predicate, roc_auc, aucroc_by_predicate


def evaluate_thresholds(model: Model, kg_val: KnowledgeGraph) -> Dict[str, float]:
    """
    Setup the threshold value for the given model & KG.
    This function mimics the `torchkge.evaluation.TripletClassificationEvaluator.evaluate` function.

    :param model: The black box model that needs to be evaluated
    :param kg_val: The validation dataset used to setup thresholds.
    :return: keys = relation id, values = threshold value
    """
    from src.highbrow.trainer import get_scores

    r_idx = kg_val.relations
    sampler = PositionalNegativeSampler(kg_val)
    model_settings = get_setting('model_settings')
    b_size = model_settings['batch_size']
    neg_heads, neg_tails = sampler.corrupt_kg(b_size, get_setting('use_cuda'), which='main')
    neg_scores = get_scores(model, neg_heads, neg_tails, r_idx, b_size)
    neg_scores = torch.tensor(neg_scores)

    pos_scores = get_scores(model, kg_val.head_idx, kg_val.tail_idx, r_idx, b_size)
    pos_scores = torch.tensor(pos_scores)

    if len(neg_scores.shape) > 1:
        neg_scores = neg_scores[:, 0]
        pos_scores = pos_scores[:, 0]

    thresholds = zeros(kg_val.n_rel)

    for i in range(kg_val.n_rel):
        mask = (r_idx == i).bool()
        if mask.sum() > 0:
            pos_scores_r_idx = pos_scores[mask]
            neg_scores_r_idx = neg_scores[mask]
            scores_r_idx = torch.cat((pos_scores_r_idx, neg_scores_r_idx))
            labels_r_idx = np.concatenate((np.ones(pos_scores_r_idx.shape[0]), np.zeros(neg_scores_r_idx.shape[0])))
            if labels_r_idx.shape[0] >= 10:
                regressor = LogisticRegressionCV(class_weight='balanced')
            else:
                regressor = LogisticRegression(class_weight='balanced')

            regressor.fit(scores_r_idx.reshape(-1, 1), labels_r_idx)
            if regressor.coef_[0][0] != 0.0:
                #threshold = (1. / regressor.coef_[0][0]) * (math.log(1.5) - regressor.intercept_[0])
                threshold = - regressor.intercept_[0] / regressor.coef_[0][0]
            else :
                neg_score_max = np.percentile(neg_scores_r_idx, 90)
                pos_score_min = np.percentile(pos_scores_r_idx, 10)
                threshold = (neg_score_max + pos_score_min) / 2
            thresholds[i] = threshold
        else:
            thresholds[i] = neg_scores.max()

    return thresholds.detach_()
