from typing import Optional, Union

import torchkge
from torch.optim import Optimizer
from torchkge import KnowledgeGraph
from torchkge.data_structures import SmallKG
from torchkge.models import Model
from torchkge.utils import Trainer, TrainDataLoader
from torchkge.utils.training import TrainDataLoaderIter
from tqdm import tqdm


class CustomTrainer(Trainer):
    """
    The only purpose of this class is to be able to get the counter examples generated during the training phase.
    Apart from that, it's the exact replica of the `torchkge.utils.Trainer` class.
    """

    def __init__(self,
                 model: Model,
                 criterion: torchkge.utils.losses,
                 kg_train: KnowledgeGraph,
                 n_epochs: int,
                 batch_size: int,
                 optimizer: Optimizer,
                 sampling_type: str,
                 use_cuda: Union[bool, str]
                 ):
        super().__init__(model, criterion, kg_train, n_epochs, batch_size, optimizer, sampling_type, use_cuda)
        self.counter_examples: Optional[SmallKG] = None

    def run(self):
        if self.use_cuda in ['all', 'batch']:
            self.model.cuda()
            self.criterion.cuda()

        iterator = tqdm(range(self.n_epochs), unit='epoch')
        data_loader = CustomTrainDataLoader(self.kg_train,
                                            batch_size=self.batch_size,
                                            sampling_type=self.sampling_type,
                                            use_cuda=self.use_cuda)
        self.counter_examples = data_loader.get_counter_examples()

        for epoch in iterator:
            sum_ = 0
            for i, batch in enumerate(data_loader):
                loss = self.process_batch(batch)
                sum_ += loss

            iterator.set_description(
                'Epoch {} | mean loss: {:.5f}'.format(epoch + 1, sum_ / len(data_loader)))
            self.model.normalize_parameters()

    def get_counter_examples(self) -> Optional[SmallKG]:
        return self.counter_examples


class CustomTrainDataLoader(TrainDataLoader):
    def __init__(self, kg: KnowledgeGraph, batch_size: int, sampling_type: str, use_cuda: Union[bool, str]):
        super().__init__(kg, batch_size, sampling_type, use_cuda)
        self.iterator = TrainDataLoaderIter(self)

    def __iter__(self):
        return self.iterator

    def get_counter_examples(self) -> SmallKG:
        return SmallKG(self.iterator.nh, self.iterator.nt, self.iterator.r)
