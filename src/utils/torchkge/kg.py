import pandas as pd
from torchkge.data_structures import KnowledgeGraph


def kg2df(kg: KnowledgeGraph) -> pd.DataFrame:
    """
    Revert a torchKGE `KnowledgeGraph` into a pandas `DataFrame`.

    :param kg: A knowledge graph.
    :return: A dataframe containing the same information than the knowledge graph.
    """
    ix2rel = dict([(ix, rel) for rel, ix in kg.rel2ix.items()])
    ix2ent = dict([(ix, rel) for rel, ix in kg.ent2ix.items()])

    df = pd.DataFrame({'from': kg.head_idx, 'rel': kg.relations, 'to': kg.tail_idx})

    df['from'] = df['from'].map(ix2ent)
    df['rel'] = df['rel'].map(ix2rel)
    df['to'] = df['to'].map(ix2ent)

    return df
