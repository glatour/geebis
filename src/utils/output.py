import os
import pandas as pd
import numpy as np

from src.utils.disk_io import load_knowledge_graph


def extract_predicate_from_id(id_series: pd.Series) -> pd.Series:
    data = []
    for idx, val in id_series.iteritems():
        if val.endswith(('_0', '_1', '_2', '_3', '_4', '_5')):
            data.append(val[:-2])
        elif '->' not in val:
            data.append(val)
        else:
            data.append(val.split('->')[1])

    return pd.Series(data=data, index=id_series.index)

def compare_synthetized_results(df_noconst: pd.DataFrame = None, df_default: pd.DataFrame = None) -> pd.DataFrame:
    if df_default is None:
        return df_noconst
    elif df_noconst is None:
        return df_default

    ## At this stage both are not null
    return df_default.merge(df_noconst, on='predicate', how='outer', suffixes=('_default', '_noconst'))

def synthetize_results(file: str, dataset_name: str) -> pd.DataFrame:
    df = None
    if file is not None and os.path.getsize(file) > 1:
        df = pd.read_csv(file)

    dataset_test_df, dataset_test_kg = load_knowledge_graph(name=dataset_name, dataset='test')
    dataset_val_df, dataset_val_kg = load_knowledge_graph(name=dataset_name, dataset='valid')
    data = {'predicate': [], 'accuracy-global': [], 'precision-global': [], 'roc-auc-global': [],
            'subject-mrr-global': [], 'subject-mr-global': [], 'object-mrr-global': [], 'object-mr-global': [],
            'rules-global': [], 'not-trivial-global': [], 'accuracy-local': [], 'precision-local': [], 'roc-auc-local': [],
            'subject-mrr-local': [], 'subject-mr-local': [], 'object-mrr-local': [], 'object-mr-local': [],
            'rules-local': [], 'not-trivial-local': [], 'accuracy-cluster': [], 'precision-cluster': [], 'roc-auc-cluster': [],
            'subject-mrr-cluster': [], 'subject-mr-cluster': [], 'object-mrr-cluster': [], 'object-mr-cluster': [],
            'rules-cluster': [], 'not-trivial-cluster': [], 'support-global': [], 'support-local': [],
            'support-cluster' : [], 'predicate-cardinality-in-test': [], 'predicate-coverage-in-test': [],
            'predicate-cardinality-in-valid': [], 'predicate-coverage-in-valid-and-test': [],
            'bb-accuracy-global': [], 'bb-accuracy-local' : [], 'bb-accuracy-cluster': []
            }
    if df is not None:
        df['predicate'] = extract_predicate_from_id(df['id'])
        df_local = df[df['broadness'] == 'local']
        df_global = df[df['broadness'] == 'global']
        df_cluster = df[df['broadness'] == 'cluster']
        df_local_gb = df_local.groupby('predicate')
        df_global_gb = df_global.groupby('predicate')
        df_cluster_gb = df_cluster.groupby('predicate')
        for predicate in df['predicate'].unique():
            data['predicate'].append(predicate)
            clean_predicate = predicate.replace('>', '/')
            predicate_card_in_test = len(dataset_test_df[dataset_test_df['rel'] == clean_predicate])
            predicate_card_in_val = len(dataset_val_df[dataset_val_df['rel'] == clean_predicate])
            data['predicate-cardinality-in-test'].append(predicate_card_in_test)
            data['predicate-coverage-in-test'].append(predicate_card_in_test / len(dataset_test_df))
            data['predicate-cardinality-in-valid'].append(predicate_card_in_val)
            data['predicate-coverage-in-valid-and-test'].append(
                (predicate_card_in_test + predicate_card_in_val) / (len(dataset_test_df) + len(dataset_val_df))
            )
            if predicate in df_global_gb.groups:
                group_global = df_global_gb.get_group(predicate)
                data['support-global'].append(group_global['support'].values[0])
                data['accuracy-global'].append(group_global['context_accuracy'].values[0])
                data['precision-global'].append(group_global['highbrow_precision'].values[0])
                data['roc-auc-global'].append(group_global['context_roc_auc'].values[0])
                data['subject-mrr-global'].append(group_global['s-mrr'].values[0])
                data['subject-mr-global'].append(group_global['s-mr'].values[0])
                data['object-mrr-global'].append(group_global['o-mrr'].values[0])
                data['object-mr-global'].append(group_global['o-mr'].values[0])
                data['rules-global'].append(int(group_global['rule 1'].values[0] is not np.nan))
                data['not-trivial-global'].append(int(not group_global['rule 1'].isnull().values.any() and
                                                 not group_global['support'].isnull().values.any() and
                                                      not group_global['highbrow_positive_predictions'].isnull().values.any()))
#                                                 not group_global['highbrow_positive_predictions'].isnull().values.any()
#                                                 and ((group_global['support'] > group_global['highbrow_positive_predictions'])
#                                                          & (group_global['highbrow_positive_predictions'] > 0)).any())
                data['bb-accuracy-global'].append(group_global['black_box_accuracy'].values[0])
            else:
                data['accuracy-global'].append(np.nan)
                data['precision-global'].append(np.nan)
                data['roc-auc-global'].append(np.nan)
                data['subject-mrr-global'].append(np.nan)
                data['subject-mr-global'].append(np.nan)
                data['object-mrr-global'].append(np.nan)
                data['object-mr-global'].append(np.nan)
                data['rules-global'].append(0)
                data['not-trivial-global'].append(0)
                data['support-global'].append(0)
                data['bb-accuracy-global'].append(np.nan)

            if predicate in df_local_gb.groups:
                group_local = df_local_gb.get_group(predicate)
                not_trivial_mask = ((~group_local['rule 1'].isnull()) & \
                                   (~group_local['support'].isnull()) & \
                                    (~group_local['highbrow_positive_predictions'].isnull()))
#                                    (~group_local['highbrow_positive_predictions'].isnull()) & \
#                                   (group_local['support'] > group_local['highbrow_positive_predictions']) & \
#                                   (group_local['highbrow_positive_predictions'] > 0))

                not_trivial_count = np.count_nonzero(not_trivial_mask)
                rules_mask = ~group_local['rule 1'].isnull()
                if not_trivial_count > 0 :
                    data['support-local'].append(not_trivial_count)
                    data['accuracy-local'].append(group_local['context_accuracy'][not_trivial_mask].mean())
                    data['precision-local'].append(group_local['highbrow_precision'][not_trivial_mask].mean())
                    data['roc-auc-local'].append(group_local['context_roc_auc'][not_trivial_mask].mean())
                    data['subject-mrr-local'].append(group_local['s-mrr'][not_trivial_mask].dropna().mean())
                    data['subject-mr-local'].append(group_local['s-mr'][not_trivial_mask].dropna().mean())
                    data['object-mrr-local'].append(group_local['o-mrr'][not_trivial_mask].dropna().mean())
                    data['object-mr-local'].append(group_local['o-mr'][not_trivial_mask].dropna().mean())
                    data['rules-local'].append(np.count_nonzero(rules_mask))
                    data['bb-accuracy-local'].append(group_local['black_box_accuracy'].mean())
                else:
                    data['support-local'].append(0)
                    data['accuracy-local'].append(np.nan)
                    data['precision-local'].append(np.nan)
                    data['roc-auc-local'].append(np.nan)
                    data['subject-mrr-local'].append(np.nan)
                    data['subject-mr-local'].append(np.nan)
                    data['object-mrr-local'].append(np.nan)
                    data['object-mr-local'].append(np.nan)
                    data['rules-local'].append(0)
                    data['bb-accuracy-local'].append(0)
                data['not-trivial-local'].append(not_trivial_count)
            else:
                data['accuracy-local'].append(np.nan)
                data['precision-local'].append(np.nan)
                data['roc-auc-local'].append(np.nan)
                data['subject-mrr-local'].append(np.nan)
                data['subject-mr-local'].append(np.nan)
                data['object-mrr-local'].append(np.nan)
                data['object-mr-local'].append(np.nan)
                data['not-trivial-local'].append(0)
                data['rules-local'].append(0)
                data['support-local'].append(0)
                data['bb-accuracy-local'].append(0)

            if predicate in df_cluster_gb.groups:
                group_cluster = df_cluster_gb.get_group(predicate)
                not_trivial_mask = ((~group_cluster['rule 1'].isnull()) & \
                                   (~group_cluster['support'].isnull()) & \
                                    (~group_cluster['highbrow_positive_predictions'].isnull()))
#                                    (~group_cluster['highbrow_positive_predictions'].isnull()) & \
#                                   (group_cluster['support'] > group_cluster['highbrow_positive_predictions']) & \
#                                   (group_cluster['highbrow_positive_predictions'] > 0))

                not_trivial_count = np.count_nonzero(not_trivial_mask)
                rules_mask = ~group_cluster['rule 1'].isnull()
                if not_trivial_count > 0 :
                    data['accuracy-cluster'].append(group_cluster['context_accuracy'][not_trivial_mask].mean())
                    data['precision-cluster'].append(group_cluster['highbrow_precision'][not_trivial_mask].mean())
                    data['roc-auc-cluster'].append(group_cluster['context_roc_auc'][not_trivial_mask].mean())
                    data['subject-mrr-cluster'].append(group_cluster['s-mrr'][not_trivial_mask].dropna().mean())
                    data['subject-mr-cluster'].append(group_cluster['s-mr'][not_trivial_mask].dropna().mean())
                    data['object-mrr-cluster'].append(group_cluster['o-mrr'][not_trivial_mask].dropna().mean())
                    data['object-mr-cluster'].append(group_cluster['o-mr'][not_trivial_mask].dropna().mean())
                    data['rules-cluster'].append(np.count_nonzero(rules_mask))
                    data['support-cluster'].append(group_cluster['support'][not_trivial_mask].sum())
                    data['bb-accuracy-cluster'].append(group_cluster['context_accuracy'][not_trivial_mask].mean())
                else:
                    data['accuracy-cluster'].append(np.nan)
                    data['precision-cluster'].append(np.nan)
                    data['roc-auc-cluster'].append(np.nan)
                    data['subject-mrr-cluster'].append(np.nan)
                    data['subject-mr-cluster'].append(np.nan)
                    data['object-mrr-cluster'].append(np.nan)
                    data['object-mr-cluster'].append(np.nan)
                    data['rules-cluster'].append(0)
                    data['support-cluster'].append(0)
                    data['bb-accuracy-cluster'].append(np.nan)
                data['not-trivial-cluster'].append(not_trivial_count)
            else:
                data['accuracy-cluster'].append(np.nan)
                data['precision-cluster'].append(np.nan)
                data['roc-auc-cluster'].append(np.nan)
                data['subject-mrr-cluster'].append(np.nan)
                data['subject-mr-cluster'].append(np.nan)
                data['object-mrr-cluster'].append(np.nan)
                data['object-mr-cluster'].append(np.nan)
                data['not-trivial-cluster'].append(0)
                data['rules-cluster'].append(0)
                data['support-cluster'].append(0)
                data['bb-accuracy-cluster'].append(np.nan)

    result_df = pd.DataFrame(data)
    return result_df

def latex_header(metrics) -> str :
    output = '\\begin{table*}\n\\begin{tabular}{l'
    for i in range(6*len(metrics)):
        output += 'C{0.75cm}'
    output += '}\n'

    headers_metric = []
    output += '&'
    for metric in metrics :
        headers_metric.append('\multicolumn{6}{c}{' + metric + '}')
    output += '&'.join(headers_metric) + ' \\\\ \\cmidrule[.1em]{2-19} \n'

    output += '&'
    for id, metric in enumerate(metrics) :
        headers_rule_type = ['\multicolumn{3}{c}{Unbounded}', '\multicolumn{3}{c}{Bounded}']
        output += '&'.join(headers_rule_type)
        if id < len(metrics) - 1 :
            output += '&'
    output += ' \\\\ \\cmidrule[.05em]{2-19} \n'

    output += '&'
    for id, metric in enumerate(metrics):
        locality = ['L', 'C', 'G', 'L', 'C', 'G']
        output += '&'.join(locality)
        if id < len(metrics) - 1 :
            output += '&'
    output += ' \\\\ \\toprule'

    return output

def latex_footer() -> str:
    return '\end{tabular}\n\end{table*}'

def compare_rules(df1: pd.DataFrame, df2: pd.DataFrame):
    rule_cols = [f'rule {i}' for i in range(1, 11)]
    mask1 = (~df1['rule 1'].isnull())
    mask2 = (~df2['rule 1'].isnull())
    df1 = df1[['broadness', 'id'] + rule_cols][mask1]
    df2 = df2[['broadness', 'id'] + rule_cols][mask2]
    new_df = pd.merge(df1, df2, how='left', left_on=['broadness', 'id'], right_on=['broadness', 'id'])
    rules_xy_series = []
    rules_x__y_series = []
    rules_x_not_y_series = []
    rules_y_not_x_series = []
    n_rules_xy_series = []
    n_rules_x__y_series = []
    n_rules_x_not_y_series = []
    n_rules_y_not_x_series = []
    n_rules_x_series = []
    n_rules_y_series = []
    ## Look at the rules
    for idx, row in new_df.iterrows():
        rules_x = set()
        rules_y = set()
        for column_name in rule_cols:
            rulex = row[column_name + '_x']
            if isinstance(rulex, str) and len(rulex) > 0:
                rules_x.add(rulex)
            ruley = row[column_name + '_y']
            if isinstance(ruley, str) and len(ruley) > 0:
                rules_y.add(ruley)
        rules_xy = rules_x & rules_y
        rules_x__y = rules_x.symmetric_difference(rules_y)
        rules_x_not_y = rules_x.difference(rules_y)
        rules_y_not_x = rules_y.difference(rules_x)

        rules_xy_series.append("||".join(rules_xy))
        n_rules_xy_series.append(len(rules_xy))
        rules_x__y_series.append("||".join(rules_x__y))
        n_rules_x__y_series.append(len(rules_x__y))
        rules_x_not_y_series.append("||".join(rules_x_not_y))
        n_rules_x_not_y_series.append(len(rules_x_not_y))
        rules_y_not_x_series.append("||".join(rules_y_not_x))
        n_rules_y_not_x_series.append(len(rules_y_not_x))
        n_rules_x_series.append(len(rules_x))
        n_rules_y_series.append(len(rules_y))

    new_df['n_x_I_y'] = pd.Series(n_rules_xy_series)
    new_df['x_I_y'] = pd.Series(rules_xy_series)
    new_df['n_x_SD_y'] = pd.Series(n_rules_x__y_series)
    new_df['x_SD_y'] = pd.Series(rules_x__y_series)
    new_df['n_x_M_y'] = pd.Series(n_rules_x_not_y_series)
    new_df['x_M_y'] = pd.Series(rules_x_not_y_series)
    new_df['n_y_M_x'] = pd.Series(n_rules_y_not_x_series)
    new_df['y_M_x'] = pd.Series(rules_y_not_x_series)
    new_df['n_x'] = pd.Series(n_rules_x_series)
    new_df['n_y'] = pd.Series(n_rules_y_series)

    return new_df.drop(columns=([c + '_x' for c in rule_cols] + [c + '_y' for c in rule_cols]))
