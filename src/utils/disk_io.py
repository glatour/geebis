import json
import math
import pickle
from glob import glob
from os import PathLike, path, makedirs
from os.path import dirname, abspath, join, isdir
from typing import Dict, Optional, Any, Union, Tuple

import numpy as np
import pandas as pd
from sklearn.base import ClassifierMixin, RegressorMixin
from torchkge import KnowledgeGraph
from torchkge.exceptions import NoPreTrainedVersionError
from torchkge.models import Model
from torchkge.utils import load_fb13, load_fb15k, load_fb15k237, load_wn18, load_wn18rr, load_yago3_10

from src.utils.logger import get_logger
from src.utils.torchkge.kg import kg2df

logger = get_logger()


def root_folder() -> PathLike:
    """
    Get the root folder of the project

    :return: the root folder path
    """
    return abspath(dirname(dirname(dirname(__file__))))


def check_directory(directory: PathLike):
    """
    Create the directory if it does not exist yet.

    :param directory: The directory that needs to be checked.
    :return: None
    """
    # create folder if not exist
    if not isdir(directory):
        logger.info(f'The path f{directory} did not exist')
        makedirs(directory, mode=0o774, exist_ok=True)


def load_knowledge_graph(name: str, dataset: str = 'train') -> \
        Union[Tuple[pd.DataFrame, KnowledgeGraph],
              Tuple[Tuple[pd.DataFrame, KnowledgeGraph],
                    Tuple[pd.DataFrame, KnowledgeGraph],
                    Tuple[pd.DataFrame, KnowledgeGraph]]]:
    """
    Load the training or testing set of the knowledge graph given in parameter as a pandas dataframe

    The columns in the incoming file are:
        - subject
        - predicate
        - object
        - truth_value (optional) 1 = True, -1 = False

    :param name: The name of the knowledge graph
    :param dataset: By default the training set will be returned; `valid` and `test` will
    return the validation and test sets accordingly.
    :return: The knowledge graph as a pandas dataframe
    """
    if name in ['fb13', 'fb15k', 'fb15k237', 'fb15k-237', 'wn18', 'wn18rr', 'yago3-10'] :
        kg_train, kg_valid, kg_test = load_torchkge_knowledge_graph(name)
        if dataset == 'all' :
            df_train = kg2df(kg_train)
            df_train['is_true'] = True
            df_valid = kg2df(kg_valid)
            df_valid['is_true'] = True
            df_test = kg2df(kg_test)
            df_test['is_true'] = True
            return df_train, kg_train, \
                   df_valid, kg_valid, \
                   df_test, kg_test
        if dataset == 'valid':
            df_valid = kg2df(kg_valid)
            df_valid['is_true'] = True
            return df_valid, kg_valid
        elif dataset == 'test':
            df_test = kg2df(kg_test)
            df_test['is_true'] = True
            return df_test, kg_test
        else:
            df_train = kg2df(kg_train)
            df_train['is_true'] = True
            return df_train, kg_train

    dataset_path = abspath(join(root_folder(), f'data/knowledge_graphs/{name}'))
    if dataset == 'all':
        df_valid = pd.read_csv(f'{dataset_path}/valid.tsv', sep='\t', names=['from', 'rel', 'to', 'truth_value'])
        df_valid['is_true'] = df_valid['truth_value'].map(lambda e: math.isnan(e) or e > 0.5)
        df_train = pd.read_csv(f'{dataset_path}/train.tsv', sep='\t', names=['from', 'rel', 'to', 'truth_value'])
        df_train['is_true'] = df_train['truth_value'].map(lambda e: math.isnan(e) or e > 0.5)
        df_test = pd.read_csv(f'{dataset_path}/test.tsv', sep='\t', names=['from', 'rel', 'to', 'truth_value'])
        df_test['is_true'] = df_test['truth_value'].map(lambda e: math.isnan(e) or e > 0.5)
        df = pd.concat([df_train, df_valid, df_test])
        kg = KnowledgeGraph(df)
        kg_train, kg_valid, kg_test = kg.split_kg(sizes=(len(df_train), len(df_valid), len(df_test)))
        return df_train, kg_train, df_valid, kg_valid, df_test, kg_test
    if dataset == 'valid':
        file_name = 'valid.tsv'
    elif dataset == 'test':
        file_name = 'test.tsv'
    else :
        file_name = 'train.tsv'

    df = pd.read_csv(f'{dataset_path}/{file_name}', sep='\t', names=['from', 'rel', 'to', 'truth_value'])
    df['is_true'] = df['truth_value'].map(lambda e: math.isnan(e) or e > 0.5)
    return df, KnowledgeGraph(df=df[['from', 'rel', 'to']])



def store_model(model: Model, data: Dict, counter_examples: pd.DataFrame = None) -> None:
    """
    Create two files: the model as a pickle file and additional information as json file

    Both file are located in data/models/<model>/<kg>_<hash>.<ext> and use the information provided by `data`
    :param model: The model to store.
    :param data: Additional data relative to the model.
    :param counter_examples: The counter examples generated during the model's training.
    :return: Nothing but write 2 files
    """
    folder_path = abspath(join(root_folder(), f'data/models/{data["model"]}'))

    # create folder if not exist
    check_directory(folder_path)

    file_path = f'{folder_path}/{data["kg"]}_{data["hash"]}'

    logger.debug('Store model file')
    with open(f'{file_path}.pkl', 'wb') as handle:
        pickle.dump(model, handle)

    logger.debug('Store data file')
    with open(f'{file_path}.json', 'w') as data_file:
        json.dump(data, data_file)

    if counter_examples is not None:
        logger.debug('Store counter examples file')
        with open(f'{file_path}_counter_ex.csv', 'w') as counter_examples_file:
            counter_examples.to_csv(counter_examples_file, header=False, index=False, sep='\t')


def load_black_box_model(model_name: str, kg_name: str, hash_name: str) -> Model:
    """
    Load the black box model stored in the file `data/models/<model_name>/<kg_name>_<hash_name>.pkl`.

    Of course this call will fail if the file does not exist.

    :param model_name: The name of the black box model.
    :param kg_name: The name of the dataset.
    :param hash_name: The hash string.
    :return: a torchKGE model.
    """
    logger.debug(f'Loading black-box model {model_name} for the KG {kg_name} ({hash_name}).')
    model_file_path = path.join(root_folder(), f'data/models/{model_name}/{kg_name}_{hash_name}.pkl')

    # check if the trained model file exist and load that model if it does
    if path.isfile(model_file_path):
        with open(model_file_path, 'rb') as pickle_file:
            return pickle.load(pickle_file)
    else:
        msg = f'The combination of model "{model_name}" and KG "{kg_name}" has not been trained (yet).'
        logger.error(msg)
        raise Exception(msg)


def load_counter_examples(model_name: str, kg_name: str, hash_name: Optional[str] = None) -> pd.DataFrame:
    """
    Simple wrapper around the `pd.read_csv` function call.

    :param model_name: The name of the model.
    :param kg_name: The name of the knowledge graph.
    :param hash_name: The hash corresponding to the settings of the computation of the model.
    :return: A dataframe containing the corrupted facts used for the computation of the model.
    """
    logger.debug(f'Loading counter examples for model {model_name} and kg {kg_name}.')
    root_path = f'{root_folder()}/data/models/{model_name}/{kg_name}'
    if hash_name is None:
        files = [abspath(f) for f in glob(f'{root_path}_*') if f.endswith('_counter_ex.csv')]
        if len(files) != 1:
            msg = f'A hash must be provided for the load of the counter examples of {model_name}/{kg_name}.'
            logger.error(msg)
            raise Exception(msg)

        hash_name = files[0].replace(f'{root_path}_', '').replace('_counter_ex.csv', '')

    counter_examples_file = abspath(f'{root_path}_{hash_name}_counter_ex.csv')
    df = pd.read_csv(counter_examples_file, sep='\t', names=['from', 'rel', 'to'])
    df['is_true'] = False
    return df

def load_torchkge_knowledge_graph(kg_name) -> (KnowledgeGraph, KnowledgeGraph, KnowledgeGraph):
    """
    Load the torchKGE knowledge graphs.
    It will return 3 KG:
        * training dataset
        * validation dataset
        * testing dataset

    :param kg_name: The name of the dataset you want to get
    :return: (kg_train, kg_val, kg_test)
    """
    logger.debug(f'Loading torchkge KG {kg_name}.')
    if kg_name == 'fb13':
        load_kg = load_fb13
    elif kg_name == 'fb15k':
        load_kg = load_fb15k
    elif kg_name in ['fb15k237', 'fb15k-237']:
        load_kg = load_fb15k237
    elif kg_name == 'wn18':
        load_kg = load_wn18
    elif kg_name == 'wn18rr':
        load_kg = load_wn18rr
    elif kg_name == 'yago3-10':
        load_kg = load_yago3_10
    else:
        msg = f'The kg {kg_name} was not found in the torchkge library'
        logger.error(msg)
        raise NoPreTrainedVersionError(msg)

    return load_kg()


def store_explanation(metrics: Dict[str, Any],
                      rules: pd.DataFrame,
                      model: Union[ClassifierMixin, RegressorMixin],
                      file_identifier: str,
                      model_name: str,
                      kg_name: str,
                      hash_name: str,
                      amie_profile: str):
    """
    Store two files containing the rules and the metrics extracted from a context during the training of the highbrow
    model.

    :param metrics: The metrics (accuracy, mrr, ...) extracted during the training.
    :param rules: The rules and coefficients (called verdicts) extracted during the training.
    :param model: The prediction model used to produce the predictions evaluated in metrics
    :param file_identifier: The name of the file (should say if context was local/global + a unique identifier).
    :param model_name: The name of the black box model.
    :param kg_name: The name of the knowledge graph dataset that is used.
    :param hash_name: The hash string used to differentiate different black box model configurations
    :param amie_profile: AMIE's execution profile according
    :return: None
    """
    folder_path = f'{root_folder()}/data/results/{amie_profile}/{model_name}/{kg_name}/{hash_name}'

    # create folder if not exist
    check_directory(folder_path)

    logger.debug(f'Store metric file {file_identifier}')
    logger.debug(str(metrics))
    metrics_file_name = f'{folder_path}/metrics_{file_identifier}.json'
    with open(metrics_file_name, 'w') as metrics_file:
        json.dump(metrics, metrics_file)

    if isinstance(model.coef_[0], np.ndarray) :
        coefs = model.coef_[0]
    else :
        coefs = model.coef_

    logger.debug(f'Store rules file {file_identifier}')
    rules_file_name = f'{folder_path}/rules_{file_identifier}.csv'
    with open(rules_file_name, 'w') as rules_file:
        (rules * coefs).to_csv(rules_file, index=False)
