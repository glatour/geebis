import json
from os.path import abspath, join
from typing import Union, Dict, List

from src.utils.disk_io import root_folder


def get_setting(name: str, file: str = 'global') -> Union[Dict, List, str]:
    """
    Retrieve a setting in the json file named by the `file` parameter.

    All settings are expected to be stored as json files in the `data/settings` folder.
    This function allows to get a stored settings from a file.

    :param name: The name of the setting
    :param file: The file in which is stored the setting. Do not add the `.json` extension.
    :return: The value of the setting, either as a dict, a list or a string
    """
    file_path = abspath(join(root_folder(), f'data/settings/{file}.json'))

    with open(file_path, 'r') as setting_file:
        data = json.load(setting_file)
    if data is None:
        raise Exception('Empty setting file')

    # If the setting is a string, replace the following variables
    variables = {
        '__ROOT_PATH__': root_folder(),
    }
    if type(data[name]) == str:
        for key, value in variables.items():
            data[name] = data[name].replace(key, value)

    return data[name]


def get_model_setting(model: str, kg: str) -> Dict:
    """
    Specific case of `get_setting` for a model and a knowledge graph.

    The settings defined in `global.json` > `models` can be overridden by the ones in `models.json`.

    :param model: The name of the model we want the settings from
    :param kg: The name of the knowledge graph for which we want the model's settings
    :return: A dict containing the settings
    """
    global_model_setting = get_setting('model_settings', 'global')
    model_setting = get_setting(model, 'models')[kg]
    return {**global_model_setting, **model_setting}
