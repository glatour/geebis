#!/usr/bin/python3

"""
Gee Explain

Usage:
    explain <model> <dataset> [options] [--verbose | -v | -vv | -vvv]
    explain -h | --help

Arguments:
    <dataset>           Any of the directory names under resources/data.
                        The program automatically reads the file train.tsv
    <model>            hole|rescal|transe| ?

Options:
    --local -l          Perform a local analysis instead of a global one.
                        Local analysis means that only the neighbourhood of the fact will be taken into account for
                        context generation.
    --clustering -c     If the analysis is local, the neighborhood of a fact is computed using clustering techniques
                        on the embeddings of the entities.
    --a-profile=<prof>  [default: default] AMIE's execution profile (e.g., default, noconst) as defined in settings/amie.json
    --log-file=<path>   If provided, the logs will (also) be written in this file.
    --torch-kge         Use torchKGE pretrained models.
                        This implies the creation of counter-examples that were not used during the training phase.
    --quiet -q          Do not print anything in STDOUT (to keep logs, please use the --log-file option)
                        Please note that Critical/Error levels will still be outputted to STDERR.
    --verbose -v        Display more or less information (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help           Show this screen.
"""
import os
import sys

# Ugly hack to allow the usage of 'src' as a python package, so I can use intellij linting correctly
root_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, root_directory)
# end of ugly hack

from src.highbrow.trainer import HighbrowTrainer

if __name__ == '__main__':
    from docopt import docopt

    from utils.logger import configure_logger

    arguments = docopt(__doc__)
    logger = configure_logger(arguments['--verbose'], arguments['--log-file'], arguments['--quiet'])

    trainer = HighbrowTrainer(arguments['<model>'], arguments['<dataset>'], arguments['--torch-kge'])
    trainer.train(arguments['--local'], arguments['--a-profile'], arguments['--clustering'])
