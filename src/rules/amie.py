"""
AMIE is a system to mine Horn rules on knowledge bases.

The program has been coded in Java and here's some binding to use this work with python.
Included libraries are stored in the `lib` folder but you can change them by tweaking the
`data/settings/amie.json` file.
"""
import csv
import subprocess
import tempfile
from typing import Any, Dict, List

import pandas as pd

from src.utils.logger import get_logger
from src.utils.misc import negate_predicate
from src.utils.settings import get_setting

logger = get_logger()

BINARY_SCORE_COLUMN = 'binarized_score'


def get_amie_settings(profile: str = 'default') -> Dict:
    """
    Get the AMIE settings needed for this run.
    :return: a dict containing
        * const     bool    if constant are enabled or not
        * fconst    bool    if constants are enforced
        * pm        string  'support' | 'headcoverage'
        * mins      int     minimum support
        * maxad     int     atoms
        * minpca    float   ?
        * minc      float   ?
        * minis     int     The minimal number of triples that the head predicate in a rule must have to be considered
                            for rule mining
        * bias      string  'amie.mining.assistant.experimental.ILPMiningAssistant' |
                            'amie.mining.assistant.experimental.ILPMiningAssistantShortConstantRules'
    """
    default = get_setting(profile, "amie")
    return default


def mine(kg: pd.DataFrame, around_predicates: List[str], amie_profile: str = 'default') -> List[Dict[str, Any]]:
    """
    Extract rules from a kg.

    The KG MUST contain at least the following columns: ['from', 'rel', 'to'].

    The KG MAY contain the following columns : ['binarized_score', 'is_true'].
    Respectively those columns are the binary score given by the black box and the ground truth of the considered fact.

    :param kg: A knowledge graph containing facts as a panda dataframe.
    :param around_predicates: The predicates around which AMIE is supposed to mine.
    :param amie_profile: The name of the AMIE configuration defined in the file settings/amie.json
    :return: Rules with metrics associated to them.
    """
    # sanity check. The given dataframe MUST contains the required columns.
    logger.debug('Launching rule mining.')
    if not {'from', 'rel', 'to'}.issubset(kg.columns):
        msg = f'The given dataframe miss the required columns (from, rel, to) for AMIE rule mining\n({kg.columns})'
        logger.error(msg)
        raise Exception(msg)

    # get AMIE java executable files
    executable_path = get_setting('rule_mining', 'amie')
    amie_settings = get_amie_settings(amie_profile)

    # format the kg to be usable by AMIE
    kg = prepare_kg(kg)

    # gather arguments
    predicates = ','.join([p for p in around_predicates if (kg['rel'] == p).any()])
    predicate_arg = ['-htr', f'"{predicates}"', '-bexr', f'"{predicates}"'] if predicates != '' else []
    optional_args = [[f'-{key}', f'{value}'] for key, value in amie_settings.items() if type(value) != bool]
    optional_args = [arg for arg in [val for args in optional_args for val in args]]
    boolean_args = [f'-{key}' for key, value in amie_settings.items() if type(value) == bool and value]
    args = ['java', '-jar', executable_path, *optional_args, *predicate_arg, *boolean_args]

    with tempfile.NamedTemporaryFile(mode='w+') as tmp_file:
        kg.to_csv(tmp_file, header=False, index=False, sep="\t")
        tmp_file.seek(0)

        args.append(tmp_file.name)
        logger.debug(f'Executing Java:\n{" ".join(args)}')
        try:
            completed_process = subprocess.run(args=args,
                                               stdout=subprocess.PIPE,
                                               stderr=subprocess.STDOUT,
                                               timeout=get_setting('rule_timeout', 'amie'),
                                               text=True)
            lines = completed_process.stdout.split('\n')
        except subprocess.TimeoutExpired as e:
            lines = e.stdout.decode("utf-8").split('\n')
        except Exception as exc:
            logger.error(f'AMIE mining encountered some error:\n{exc}')
            return []

        rules = []
        header = 'Rule\tHead Coverage\tStd Confidence\tPCA Confidence\tPositive Examples\tBody size\tPCA Body size\tFunctional variable'
        alternative_header = f'Starting the mining phase... {header}'

        if header in lines:
            beginning_index = lines.index(header) + 1
        elif alternative_header in lines:
            beginning_index = lines.index(alternative_header) + 1
        else:
            logger.error(f'Amie mining wasn\'t used correctly\n{lines}')
            return rules

        for line_components in [line.split('\t') for line in lines[beginning_index:] if line != '']:

            # if we reach this string, it means that we've mined all the possible rules and it's time to stop.
            if line_components[0].startswith('Mining done in'):
                break

            if len(line_components) < 5:
                break

            # [0] Rule, [1] Head Coverage, [2] Std Confidence, [3] PCA Confidence,
            # [4] Positive Examples, [5] Body size, [6] PCA Body size, [7] Functional variable
            rules.append({'rule': line_components[0],
                          'head-coverage': line_components[1],
                          'std-conf': line_components[2],
                          'pca-conf': line_components[3],
                          'positive-examples': line_components[4],
                          })

    logger.debug('AMIE mined ' + str(len(rules)) + ' rules')
    return rules


def evaluate(rules: List[Dict], learning_context: pd.DataFrame, global_context: pd.DataFrame) -> pd.DataFrame:
    """
    Compute rules evaluation with the help of a java program.

    Warning: The learning_context and the global_context MUST have been prepared via the `prepare_kg` function.

    It returns
        (i) the aggregated score of the explanation in the set of instances in the context,
    and (ii) the score of the instance at the center of the vicinity defined by that context

    :param rules: The rules mined by amie.
    :param learning_context: The facts for which we want an evaluation trough the amie evaluator.
    :param global_context: The known facts.
    :return pd.Dataframe: rows are facts and columns are rules.
    """
    logger.debug('Launching rule evaluator.')
    # for rule evaluation, only keep
    #   - the rule itself
    #   - the support of that rule
    #   - the confidence in that rule
    amie_rules = [(r['rule'], r['positive-examples'], r['std-conf']) for r in rules]

    with tempfile.NamedTemporaryFile(mode='w+', encoding='utf-8') as t_kb_file:
        global_context.to_csv(t_kb_file, header=False, index=False, sep="\t")
        t_kb_file.seek(0)

        with tempfile.NamedTemporaryFile(mode='w+', encoding='utf-8') as t_file:
            learning_context.to_csv(t_file, header=False, index=False, sep="\t")
            t_file.seek(0)

            with tempfile.NamedTemporaryFile(mode='w+', encoding='utf-8') as t_rules_file:
                csv_rules_writer = csv.writer(t_rules_file, delimiter='\t')
                csv_rules_writer.writerows([rule for rule in amie_rules])
                t_rules_file.seek(0)

                args = [
                    'java', '-jar',
                    get_setting('rule_eval', 'amie'),
                    t_kb_file.name,
                    t_file.name,
                    t_rules_file.name,
                    'false'
                ]
                logger.debug(f'Executing Java:\n{" ".join(args)}')

                try:
                    t = subprocess.run(args,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT,
                                       timeout=get_setting('rule_timeout', 'amie'),
                                       text=True)
                    lines = t.stdout.split("\n")
                except subprocess.TimeoutExpired as e:
                    logger.error(f'{e}')
                    lines = []
                except Exception as e:
                    logger.error(f'Problem during evaluation.\n{e}')
                    raise e

    # The first 4 lines correspond to
    # Loading files...
    #   Starting <file_name>
    #   Finished <file_name>, still running: 0
    # Loaded <number> facts in <time> s using <memory> MB
    verdicts = []
    try:
        if len(lines) >= 5 and len(rules) > 0:
            for line in lines[4:]:
                if len(line) > 0:
                    verdicts.append([float(v) for v in line.split('\t')])
    except:
        pass

    if len(verdicts) != len(learning_context) or len(verdicts[0]) != len(rules):
        logger.error('The matrix shape does not fit the input (learning context and rules).')

    df = pd.DataFrame(verdicts, columns=[r['rule'] for r in rules])
    logger.info(f'The shape of THE matrix is {df.shape}')
    return df


def prepare_kg(kg: pd.DataFrame) -> pd.DataFrame:
    """
    AMIE needs some preparation to be able to mine rules from a knowledge graph.

    First of all the facts that are known to be false need their predicate to be adapted to explicitly involve the
    negation.

    Then only 3 columns should be sent to AMIE and the order is important: subject, predicate, object.
    :param kg: The dataframe containing the knowledge graph.
    :return: A dataframe that can be used by AMIE to mine rules.
    """
    logger.debug('Preparing a dataframe for amie.')

    predicted_info_mask = None
    predicted_value_mask = None
    reality_info_mask = None
    reality_value_mask = None
    mask = None

    # The relations that are labelled as false by the black box needs to be negated for AMIE
    if BINARY_SCORE_COLUMN in kg.columns:
        truth_mask = kg[BINARY_SCORE_COLUMN] == False
        # take the 7 first because it can be '__<neg_', which is 7 character long
        neg_rel_mask = kg['rel'].apply(lambda s: 'neg_' in s[:7])
        predicted_value_mask = truth_mask & ~neg_rel_mask
        predicted_info_mask = kg[BINARY_SCORE_COLUMN].apply(lambda e: type(e) == bool)

        # The relations that are known to be false have to be negated for AMIE
    if 'is_true' in kg.columns:
        truth_mask = kg['is_true'] == False
        neg_rel_mask = kg['rel'].apply(lambda s: 'neg_' in s[:7])
        reality_value_mask = truth_mask & ~neg_rel_mask
        reality_info_mask = kg['is_true'].apply(lambda e: type(e) == bool)

    # setup final mask
    if predicted_info_mask is not None and predicted_value_mask is not None and \
            reality_info_mask is not None and reality_value_mask is not None:
        mask = (predicted_info_mask & predicted_value_mask) | (
                ~predicted_info_mask & reality_info_mask & reality_value_mask)
    elif predicted_info_mask is not None and predicted_value_mask is not None:
        mask = predicted_info_mask & predicted_value_mask
    elif reality_info_mask is not None and reality_value_mask is not None:
        mask = reality_info_mask & reality_value_mask

    if mask is not None:
        kg.loc[mask, 'rel'] = kg.loc[mask, 'rel'].apply(lambda r: negate_predicate(r))

    # AMIE needs only three column, in this particular order
    return kg[['from', 'rel', 'to']]
