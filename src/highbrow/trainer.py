import hashlib
import json
from typing import List, Iterator, Tuple

import numpy as np
import pandas as pd
import torch
from scipy.special import expit
from sklearn.cluster import AgglomerativeClustering, DBSCAN
from sklearn.linear_model import LogisticRegression, Ridge, LogisticRegressionCV
from sklearn.metrics import euclidean_distances
from sklearn.model_selection import train_test_split
from torch import Tensor
from torchkge import KnowledgeGraph, TransEModel
from torchkge.data_structures import SmallKG
from torchkge.models import Model, ComplExModel
from torchkge.sampling import BernoulliNegativeSampler, PositionalNegativeSampler
from torchkge.utils import DataLoader
from tqdm import tqdm

from src.rules import amie
from src.utils.disk_io import load_knowledge_graph, load_counter_examples, load_torchkge_knowledge_graph, \
    load_black_box_model, store_explanation
from src.utils.evaluation import link_prediction_evaluation, triplet_classification_evaluation
from src.utils.logger import get_logger
from src.utils.settings import get_model_setting, get_setting
from src.utils.torchkge.evaluation.triplet_classification import evaluate_thresholds
from src.utils.torchkge.kg import kg2df

logger = get_logger()


class HighbrowTrainer:
    def __init__(self, model: str, kg_name: str, use_pretrained_model: bool):
        """
        Constructor of the `HighbrowTrainer`

        This will fail if the selected black box model is not already trained.
        Please use the `train.py list` script or the `black_box.list.available_combinations` function to check which
        models are already trained.

        :param model: The model name
        :param kg_name: The dataset name (knowledge graph)
        :param use_pretrained_model: Force the usage of a torchKGE pretrained model
        """
        # store names, just in case
        self.model_name = model
        self.kg_name = kg_name

        # load settings and knowledge graph
        self.settings = get_model_setting(model, kg_name)
        self.training_df, self.training_kg, self.validation_df, self.validation_kg, \
        self.testing_df, self.testing_kg = load_knowledge_graph(self.kg_name, dataset='all')
        self.val_test_df = pd.concat([self.testing_df, self.validation_df], ignore_index=True)
        self.val_test_kg = KnowledgeGraph(self.val_test_df)

        if use_pretrained_model:
            hash_name = 'pretrained'
            # generate new counter examples instead of the ones used by the black-box model during training
            sampler = BernoulliNegativeSampler(self.training_kg)
            neg_heads, neg_tails = sampler.corrupt_kg(self.settings['batch_size'], get_setting('use_cuda'))
            self.counter_examples = kg2df(KnowledgeGraph(kg={'heads': neg_heads,
                                                             'tails': neg_tails,
                                                             'relations': self.training_kg.relations},
                                                         ent2ix=self.training_kg.ent2ix,
                                                         rel2ix=self.training_kg.rel2ix))
            self.counter_examples['is_true'] = False

        else:
            # todo : add validation set in every dataset you want to work with
            # todo : be careful to link every KG together (look at the torchkge datasets loading)
            #raise NotImplementedError("has to be reviewed (need a validation set)")
            #self.training_df = load_knowledge_graph(kg_name, dataset='train')
            #self.testing_df = load_knowledge_graph(kg_name, dataset='test')

            # get the file where the model is stored
            hash_name = hashlib.sha256(json.dumps({'name': model, 'kg': kg_name, **self.settings}).encode()).hexdigest()
            self.counter_examples = load_counter_examples(self.model_name, self.kg_name, hash_name)

        self.hash_name = hash_name
        self.black_box_model = load_black_box_model(self.model_name, self.kg_name, hash_name)
        self.thresholds = evaluate_thresholds(self.black_box_model, self.training_kg)

    def local_context_dataframes(self, clustering: bool = False) -> Iterator[Tuple[str, pd.DataFrame]]:
        """
        Generator of dataframes.
        Each dataframe is filled with the neighbours of a peculiar fact of the *testing* dataframe.
        These dataframes can be given to the constructor of `KnowledgeGraph`.

        This step is crucial for our framework and is linked to the creation of a context C.
        :param clustering: If true the context is computed using clustering techniques on the embeddings
                            of the entities of the facts.
        :return: iterator of tuples (file_identifier, dataframe)
        """
        if clustering :
            yield from self.local_clustering_context_dataframes()
        else:
            yield from self.local_no_clustering_context_dataframes()

    def get_entity_embedding(self, emb_id):
        if not isinstance(self.black_box_model, ComplExModel):
            return self.black_box_model.ent_emb.weight[emb_id]
        else:
            return torch.cat((self.black_box_model.re_ent_emb.weight[emb_id],
                              self.black_box_model.im_ent_emb.weight[emb_id])
                             )

    def get_clustering_parameters_bb(self, N):
        return {'n_clusters': N}

    def optimize_clustering_params(self, X):
        best_N = 2
        for N in range(2, 6):
            clustering = AgglomerativeClustering(**self.get_clustering_parameters_bb(N))
            try:
                cluster_ids = clustering.fit_predict(X)
            except:
                continue
            values, frequencies = np.unique(cluster_ids, return_counts=True)
            frequencies = np.array(frequencies)
            if ((frequencies >= 10).any()):
                best_N = N

        return self.get_clustering_parameters_bb(best_N)

    def local_clustering_context_dataframes(self):
        logger.debug('Generating local contexts via clustering.')
        for relation in self.testing_df.rel.unique():
            rel_mask = self.val_test_df['rel'] == relation
            facts_in_rel = self.val_test_df.loc[rel_mask]
            if sum(rel_mask) < 2:
                logger.warning(f'There are not enough facts for clustering for relation {relation}')
                return

            X = []
            for i in facts_in_rel.index:
                head = self.val_test_df.at[i, 'from']
                tail = self.val_test_df.at[i, 'to']
                head_emb_idx = self.training_kg.ent2ix[head]
                tail_emb_idx = self.training_kg.ent2ix[tail]
                head_emb = self.get_entity_embedding(head_emb_idx)
                tail_emb = self.get_entity_embedding(tail_emb_idx)
                X.append(torch.cat((head_emb, tail_emb)).detach().numpy())

            clustering = AgglomerativeClustering(**self.optimize_clustering_params(X))
            cluster_ids = clustering.fit_predict(X)
            unique_clusters_ids = np.unique(cluster_ids)
            n_clusters = len(unique_clusters_ids)
            logger.info(f'Relation {relation} has been grouped into {n_clusters} clusters')
            for cluster_id in unique_clusters_ids:
                idxs = np.where(cluster_ids == cluster_id)
                identifier = f'cluster_{relation}_{cluster_id}'.replace(' ', '').replace('/', '>')
                yield identifier, facts_in_rel.iloc[idxs]

    def local_no_clustering_context_dataframes(self) :
        logger.debug('Generating local contexts.')
        performed_masks = []
        for i in range(self.testing_df.shape[0]):
            relation = self.testing_df.at[i, 'rel']
            head = self.testing_df.at[i, 'from']
            tail = self.testing_df.at[i, 'to']

            rel_mask = self.val_test_df['rel'] == relation
            head_mask = self.val_test_df['from'] == head
            tail_mask = self.val_test_df['to'] == tail
            mask = rel_mask & (head_mask | tail_mask)

            sparse_mask = list(mask[mask].index)
            # check that we wont send two times the same dataframe
            if sparse_mask in performed_masks:
                logger.debug("local_context_dataframes : Skipping an already treated mask")
                continue
            else:
                performed_masks.append(sparse_mask)

            n_facts = sum(mask)
            if n_facts < 10:
                logger.warning(f'Only {n_facts} fact(s) in the local context of "{relation}({head},{tail})". ')
                logger.warning(f'Moving to the next fact.')
                continue

            fact = '->'.join([str(head), relation, str(tail)])
            identifier = f'local_{fact}'.replace(' ', '').replace('/', '>')
            yield identifier, self.val_test_df.loc[mask]

    def global_context_dataframes(self) -> Iterator[Tuple[str, pd.DataFrame]]:
        """
        Generator of dataframes.
        Each dataframe only contains facts around the same predicate.
        These dataframes can be given to the constructor of `KnowledgeGraph`.

        :return: iterator of tuples (file_identifier, dataframe)
        """
        logger.debug('Generating global contexts.')
        for relation in self.val_test_df.rel.unique():
            identifier = f'global_{relation}'.replace(' ', '').replace('/', '>')
            yield identifier, self.val_test_df.loc[self.val_test_df["rel"] == relation]

    def binarize(self, context: pd.DataFrame) -> pd.Series:
        """
        Use the thresholds computed in the constructor to label a score with a truth value.

        :param context:
        :return:
        """
        context['threshold'] = context['rel'].apply(lambda r: self.thresholds.tolist()[self.val_test_kg.rel2ix[r]])
        return context['score'] >= context['threshold']

    def normalize(self, context: pd.DataFrame) -> pd.Series:
        """
        Use the thresholds computed in the constructor to compute a probability score.

        :param context:
        :return:
        """
        context['threshold'] = context['rel'].apply(lambda r: self.thresholds.tolist()[self.val_test_kg.rel2ix[r]])
        return expit(context['score'] - context['threshold'])

    def train(self, locally: bool = False, amie_profile: str = 'default', clustering: bool = False):
        """
        Wrapper to train X linear models by context.

        Each model is specific to a certain task:
            * triplet classification
            * ranking

        Each model uses rules as input to achieve the task.
        So a list of weighted rules impacting the result provide an explanation for the mimicked black box model.
        :param locally: Whether the context is local or global.
                            * A local context is constructed with the neighborhood of a considered fact
                            * A global context is constructed with all the fact having a predicate in common.
        :param amie_profile: AMIE's execution profile (`default` allows instantiated atoms, i.e., rules with constants,
                            `noconst` does not allow instantiated atoms
        :param clustering: If the context is local, it will be computed using clustering techniques on the embeddings
                            of the entities of the fact.
        :return:
        """

        if locally:
            contexts = self.local_context_dataframes(clustering)
        else:
            contexts = self.global_context_dataframes()

        for file_identifier, df in tqdm(contexts, desc='Contexts'):
            kg = KnowledgeGraph(df=df,
                                ent2ix=self.training_kg.ent2ix,
                                rel2ix=self.training_kg.rel2ix)

            # generate counter examples
            sampler = PositionalNegativeSampler(kg)
            neg_heads, neg_tails = sampler.corrupt_kg(self.settings["batch_size"], get_setting("use_cuda"))

            # use model to label scores
            neg_scores = get_scores(self.black_box_model,
                                    neg_heads,
                                    neg_tails,
                                    kg.relations,
                                    self.settings["batch_size"])

            pos_scores = get_scores(self.black_box_model,
                                    kg.head_idx,
                                    kg.tail_idx,
                                    kg.relations,
                                    self.settings["batch_size"])

            # join everything in a dataframe
            neg_df = kg2df(KnowledgeGraph(kg={'heads': neg_heads,
                                              'tails': neg_tails,
                                              'relations': kg.relations},
                                          ent2ix=kg.ent2ix,
                                          rel2ix=kg.rel2ix,
                                          ))
            neg_df['score'] = neg_scores
            neg_df['is_true'] = False

            pos_df = kg2df(kg)
            pos_df['score'] = pos_scores
            pos_df['is_true'] = True

            context = pd.concat([pos_df, neg_df], axis=0)

            logger.debug(f'The context contains {context.shape[0]} facts.')

            # normalize & binarize
            context['normalized_score'] = self.normalize(context)
            context['binarized_score'] = self.binarize(context)

            # prefix the predicate in the context C with '__'
            context['rel'] = context['rel'].map(lambda rel: f'__{rel}')

            # split for training/testing
            context_train, context_test = train_test_split(context,
                                                           test_size=get_setting("highbrow_test_size"),
                                                           shuffle=True)

            # mine rules from K U C_train with K = { KG_train U NKG_train }, with NKG_train the counter examples
            # generated while training the black-box model.
            kg_train = pd.concat([self.counter_examples, self.training_df], axis=0)

            amie_kg_train = amie.prepare_kg(kg_train)
            amie_context_train = amie.prepare_kg(context_train.copy())
            amie_context_test = amie.prepare_kg(context_test.copy())
            rules = amie.mine(pd.concat([amie_context_train, amie_kg_train], axis=0),
                              list(set(amie_context_train['rel'].unique())), amie_profile=amie_profile)

            # calculate verdicts
            verdicts_train = amie.evaluate(rules, amie_context_train, amie_kg_train)
            verdicts_test = amie.evaluate(rules, amie_context_test, amie_kg_train)
            if len(verdicts_train) == 0 or len(verdicts_test) == 0:
                logger.warning(f'There is nothing AMIE can mine from {file_identifier}')
                continue

            # train the regressor
            if len(set(context_train['binarized_score'])) < 2:
                regressor = Ridge()
                logger.debug('Fitting a linear regressor (normalized_score).')
                regressor.fit(verdicts_train, context_train['normalized_score'])
                normalized_approximation = regressor.predict(verdicts_test)
            else :
                regressor = LogisticRegression(class_weight='balanced')
                logger.debug('Fitting the logistic regressor (binarized_score).')
                regressor.fit(verdicts_train, context_train['binarized_score'])
                # predict results
                normalized_approximation = regressor.predict_proba(verdicts_test)[:,1]

            # extract some metrics from the predictions
            # with the linear regression it's possible to do both triplet classification and ranking
            link_prediction_metrics_norm = link_prediction_evaluation(normalized_approximation,
                                                                      context_test)
            triplet_classification_metrics_norm = triplet_classification_evaluation(normalized_approximation,
                                                                                    context_test)
            metrics = {'link_prediction': link_prediction_metrics_norm,
                       'triplet_classification': triplet_classification_metrics_norm}

            store_explanation(metrics, verdicts_test, regressor, file_identifier, self.model_name,
                              self.kg_name, self.hash_name, amie_profile)


def get_scores(model: Model, heads: Tensor, tails: Tensor, relations: Tensor, batch_size: int) -> List[float]:
    """
    Copy of the `TripletClassificationEvaluator.get_scores` function.

    This copy is needed because the original function is not static and therefore cannot be reused without an instance
    of a `TripletClassificationEvaluator` which is not needed.

    :param model: The model that will be used to score the given facts.
    :param heads: The tensor of the heads of the new KG that we want to analyse through the model.
    :param tails: The tensor of the tails of the new KG that we want to analyse through the model.
    :param relations: The tensor of the relations of the new KG that we want to analyse through the model.
    :param batch_size: The batch size that will be used to iterate through the new KG.
    :return: The score list of the given facts.
    """
    small_kg = SmallKG(heads, tails, relations)
    use_cuda = 'batch' if get_setting('use_cuda') else None
    dataloader = DataLoader(small_kg, batch_size=batch_size, use_cuda=use_cuda)
    scores = []
    for batch in dataloader:
        h_idx, t_idx, r_idx = batch[0], batch[1], batch[2]
        s = model.scoring_function(h_idx, t_idx, r_idx)
        scores.extend(s.detach().numpy())

    return scores
