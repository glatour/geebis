import os
import sys

# Ugly hack to allow the usage of 'src' as a python package, so I can use intellij linting correctly
root_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, root_directory)
# end of ugly hack

import pandas as pd

models = ['transe']
datasets = ['wn18rr', 'fb15k237']

results = []
for m in models:
    for d in datasets:
        path = f'{root_directory}/data/results/{m}/{d}/pretrained/all_in_one_summary.csv'
        summary = pd.read_csv(path)
        summary = summary[summary['mrr'].notna()]
        local_rows = summary[summary['broadness'] == 'local']

        tab = {}
        local_surrogate_accuracy_score = 0
        local_black_box_accuracy_score = 0
        local_mrr_score = 0
        local_pop = 0
        for r in local_rows.iterrows():
            r = r[1]
            predicate = r['id'].split('->')[1].replace('>', '/')
            support = r['support']
            black_box_accuracy = r['black_box_accuracy']
            surrogate_accuracy = r['context_accuracy']
            mrr = r['mrr']
            if predicate not in tab:
                tab[predicate] = [(surrogate_accuracy, support, mrr, black_box_accuracy)]
            else:
                tab[predicate].append((surrogate_accuracy, support, mrr, black_box_accuracy))

            local_surrogate_accuracy_score += surrogate_accuracy * support
            local_black_box_accuracy_score += black_box_accuracy * support
            local_mrr_score += mrr * support
            local_pop += support

        if local_pop < 1:
            print(f'Avoiding zero division, no weighed average for all local! ({m} > {d})')
        results.append((m, d, 'local_avg', local_surrogate_accuracy_score / local_pop,
                        local_black_box_accuracy_score / local_pop, local_mrr_score / local_pop, local_pop))

        for predicate, items in tab.items():
            surrogate_accuracy_score = sum([i[0] * i[1] for i in items])
            black_box_accuracy_score = sum([i[3] * i[1] for i in items])
            mrr_score = sum(i[2] * i[1] for i in items)
            pop = sum(i[1] for i in items)
            if pop < 1:
                print(f'Avoiding zero division, no weighed average for relation {predicate}! ({m} > {d})')
                continue
            sur_acc_avg = surrogate_accuracy_score / pop
            bb_acc_avg = black_box_accuracy_score / pop
            mrr_avg = mrr_score / pop

            results.append((m, d, predicate, sur_acc_avg, bb_acc_avg, mrr_avg, pop))

        global_surrogate_accuracy_score = 0
        global_black_box_accuracy_score = 0
        global_mrr_score = 0
        global_pop = 0
        for r in summary[summary['broadness'] == 'global'].iterrows():
            r = r[1]
            s = r['support']
            global_surrogate_accuracy_score += r['context_accuracy'] * s
            global_black_box_accuracy_score += r['black_box_accuracy'] * s
            global_mrr_score += r['mrr'] * s
            global_pop += s

        if global_pop < 1:
            print(f'Avoiding zero division, no weighed average for all global! ({m} > {d})')
        else:
            results.append(
                (m, d, 'global_avg', global_surrogate_accuracy_score / global_pop,
                 global_black_box_accuracy_score / global_pop, global_mrr_score / global_pop, global_pop))

        all_table_black_box_accuracy_avg = sum(summary['black_box_accuracy'] * summary['support']) / sum(
            summary['support'])
        all_table_surrogate_accuracy_avg = sum(summary['context_accuracy'] * summary['support']) / sum(
            summary['support'])
        all_table_mrr_avg = sum(summary['mrr'] * summary['support']) / sum(summary['support'])
        results.append((m, d, 'all_table_avg', all_table_surrogate_accuracy_avg, all_table_black_box_accuracy_avg,
                        all_table_mrr_avg, sum(summary['support'])))

print("\n\n")
print("model,dataset,relation,accuracy(surrogate),accuracy(black-box),mrr,support")
for r in results:
    print(f'{r[0]},{r[1]},{r[2]},{r[3]},{r[4]},{r[5]},{r[6]}')
