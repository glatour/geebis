#!/usr/bin/python3

"""
Gee Analyse

Usage:
    analyse <model> <dataset> <hash> <profile> [options] [--verbose | -v | -vv | -vvv]
    analyse -h | --help

Arguments:
    <dataset>           Any of the directory names under resources/data.
                        The program automatically reads the file train.tsv
    <model>             hole|rescal|transe| ?
    <hash>              The hash allowing to assert which settings were used during training 
    <profile>           [default: default] AMIE's execution profile
Options:
    --log-file=<path>   If provided, the logs will (also) be written in this file.
    --quiet -q          Do not print anything in STDOUT (to keep logs, please use the --log-file option)
                        Please note that Critical/Error levels will still be outputted to STDERR.
    --verbose -v        Display more or less information (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help           Show this screen.
"""
import os
import sys

# Ugly hack to allow the usage of 'src' as a python package, so I can use intellij linting correctly
root_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, root_directory)
# end of ugly hack

from src.analysis.explanation import parse_results
from src.utils.disk_io import root_folder

if __name__ == '__main__':
    from docopt import docopt

    from utils.logger import configure_logger

    arguments = docopt(__doc__)
    logger = configure_logger(arguments['--verbose'], arguments['--log-file'], arguments['--quiet'])

    results = parse_results(arguments['<profile>'], arguments['<model>'], arguments['<dataset>'], arguments['<hash>'])
    folder = f'{root_folder()}/data/results/{arguments["<profile>"]}/{arguments["<model>"]}/{arguments["<dataset>"]}/{arguments["<hash>"]}'
    if not os.path.exists(folder):
        os.makedirs(folder)

    results.to_csv(f'{folder}/all_in_one_summary.csv', index=False)