#!/usr/bin/python3

"""
Gee Train

The black box models get trained here.

The `list` command displays available models and datasets, and if there is an already trained model for that dataset.

Usage:
    train <model> <dataset> [options] [--verbose | -v | -vv | -vvv]
    train list
    train -h | --help

Arguments:
    <dataset>           Any of the directory names under resources/data.
                        The program automatically reads the file train.tsv
    <model>            hole|rescal|transe| ?

Options:
    -f --force-train    Replace existing trained model if any.
    --torch-kge         Use torchKGE pretrained model (incompatible with --force-train)
    --log-file=<path>   If provided, the logs will (also) be written in this file.
    --quiet -q          Do not print anything in STDOUT (to keep logs, please use the --log-file option)
                        Please note that Critical/Error levels will still be outputted to STDERR.
    --verbose -v        Display more or less information (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    --full-eval         If true, both link prediction and triple classification evaluations are executed. If false,
                        the triple classification evaluation is omitted. This only applies to non-pretrained torchkge models
    --skip-eval         If true, all evaluations are skipped. This only applies to non-pretrained torchkge models
    -h --help           Show this screen.
"""
import os
import sys

# Ugly hack to allow the usage of 'src' as a python package, so I can use intellij linting correctly
root_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, root_directory)
# end of ugly hack

from src.black_box.pretrained import PreTrained

if __name__ == '__main__':
    from docopt import docopt

    from src.black_box.list import available_combinations
    from src.black_box.trainer import ModelTrainer
    from utils.logger import configure_logger

    arguments = docopt(__doc__)
    logger = configure_logger(arguments['--verbose'], arguments['--log-file'], arguments['--quiet'])

    if arguments['--torch-kge'] and arguments['--force-train']:
        logger.warning('The arguments --force and --torch-kge are incompatible. Usage of --torch-kge prevails.')

    if arguments['list']:
        available_combinations()
        exit(0)

    if arguments['--torch-kge']:
        pre_trained = PreTrained(arguments['<model>'], arguments['<dataset>'])
        pre_trained.evaluate()
        pass
    else:
        trainer = ModelTrainer(arguments['<model>'], arguments['<dataset>'])

        if not trainer.is_trained or arguments['--force-train']:
            trainer.train(arguments['--skip-eval'], arguments['--full-eval'])
