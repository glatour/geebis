# Gee (bis)

> Explaining Embedding-based Models for Link Prediction in Knowledge Graphs

This repository contains the code and experimental data from the paper [*Explaining Embedding-based Models for Link
Prediction in Knowledge Graphs*](https://openreview.net/forum?id=1nEMe9QGuPS).

## Package Dependencies

You'll need java JRE correctly setup in your path (tested with openjdk 11.0.10).

For the python part, we recommend creating a [virtual environment](https://docs.python.org/3/library/venv.html) for this
project.

```shell
cd gee
python3 -m venv venv
source venv/bin/activate
```

Here's the content of the `requirements.txt` file that contains almost all the requirements

```
docopt~=0.6.2
pandas~=1.2.4
torchkge~=0.16.25
torch~=1.8.1
scipy~=1.6.3
```

You can install those with the following command :

```shell
pip install -r requirements.txt
```

## Datasets

The experimental datasets are available under `data/knowledge_graphs`. There is a directory per dataset. The data is
divided into train and test sets stored in the files `train.tsv` and `test.tsv` respectively.

By default, the training routine uses the file `train.tsv`, whereas the explainer uses `test.tsv` to learn and evaluate
the explanations.

## Training models

To train models you can use the `train.py` script for which you can find the following documentation:

```
Geebis Train

The black box models get trained here.

The `list` command displays available models and datasets, and if there is an already trained model for that dataset.

Usage:
    train <model> <dataset> [options] [--verbose | -v | -vv | -vvv]
    train list
    train -h | --help

Arguments:
    <dataset>           Any of the directory names under resources/data.
                        The program automatically reads the file train.tsv
    <model>            hole|rescal|transe| ?

Options:
    -f --force-train    Replace existing trained model if any.
    --log-file=<path>   If provided, the logs will (also) be written in this file.
    --quiet -q          Do not print anything in STDOUT (to keep logs, please use the --log-file option)
                        Please note that Critical/Error levels will still be outputted to STDERR.
    --verbose -v        Display more or less information (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help           Show this screen.
```

An example could be:

`$ python3 train.py transe fb15k-237-wcp`

## Computing Explanations

![algo](data/images/highbrow_train_algo.png)